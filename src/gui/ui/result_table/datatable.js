import {TemplateData} from 'template/components/template_data';
import {DataDefinition} from 'gui/components/data_defination';
import {Hooks} from 'gui/components/hooks';
import {ConfigData} from 'gui/components/config_data';

import {displayDatetime, displayDate} from 'base/helper/datetime_helper'; 


var Datatable = {};


var buildColumnOption = function(columnInfo){
	var option = {};
	var key  = columnInfo.group + '__' + columnInfo.name;

    var className = '';
    // format data
    var dataFormat = columnInfo.dataFormat;
    if(!dataFormat || dataFormat == 'raw') {
        option.mData = key;
    } else {
        option.render = function(data, column, full) {
            switch(dataFormat) {
                case 'currency':
                    return displayCurrency(full[key]);
                case 'date':
                    return displayDate(full[key]);
                case 'datetime':
                    return displayDatetime(full[key]);
            }
            return full[key];
        }
    }
    // end format

    // align
    if(columnInfo.align) {
        className += ' text-' + columnInfo.align; // text-right, text-left
    }
    // align

    // hide
    if(columnInfo.hide) {
		className += ' hide';
	}
    // end hide

    if(className != '') {
         option.sClass = className;
    }
   

	option.bSortable = !!columnInfo.sort; // boolean
	return option;
}

var buildColumnsOption = function(){
	// TODO: cache later
	var columnInfos = _.sortBy(TemplateData.Column.getData(), 'order');
	var result = [];
	_.each(columnInfos, function(columnInfo){
		var opt = buildColumnOption(columnInfo);
		result.push(opt);
	});

    // custom columns
    var customCols = ConfigData.getCustomColumns();
    if(!_.isEmpty(customCols)) {
        result = result.concat(customCols);
    }
    // end custom columns

    // klog(result)
	return result;
}

var fnServerData = function(searchData, filterData){
	var queryInf = DataDefinition.queryInfo(searchData);
	return function (sSource, aoData, fnCallback) {
        var sortInfo = DataDefinition.sortInfo(aoData);

        var b4 = Hooks.ResultTable.beforeRequestServer.runSequence(aoData, searchData, filterData, queryInf, sortInfo);
        if(b4 === false) {
            return;
        }

        aoData.unshift({'name': 'server_hooks', 'value': JSON.stringify(ConfigData.getServerHooks()) });

        aoData.unshift({'name': 'search_data', 'value': JSON.stringify(searchData)});
        // update date time type
        aoData.unshift({'name': 'filter_data', 'value': JSON.stringify(filterData)});
        
        // CAN BE HOOK FOR QUERY INFO
        aoData.unshift({'name': 'query_info', value: JSON.stringify(queryInf)})

        // CAN BE HOOK FOR SORT INFO
        aoData.unshift({'name': 'sort_info', value: JSON.stringify(sortInfo)})

        $.ajax({
            "dataType": "json",
            "type": "POST",
            "url": sSource,
            "data": aoData
        }).done(function (msg) {
            try {
                fnCallback(msg);
            } catch (err) {
            	console.error(err);
            }
        });
    }
}

var buildOption = function(search_data, filter_data, tabid){
	var option = {
		"destroy": true,
		"bProcessing": false,
        "bServerSide": true,
        "bStateSave": false,
        "bLengthChange": false,
        "iDisplayLength": parseInt(max_item_per_page, 10), // from global variable
        // "order": [[5, "asc"]], // col 5 is Purchase Order ID
   		"dom": '<"top"ip>rt<"bottom"fl><"clear">',
   		"bPaginate": true,
        "bScrollCollapse": true,
        "sAjaxSource": base_url + "eform/searchservice/test_data",
        "fnServerData": fnServerData(search_data, filter_data),
        "fnDrawCallback": function (oSettings) {
            var opt = ConfigData.getDoubleClick();
            if(!_.isFunction(opt.onDoubleClick)) {
                return;
            }
            var tableID = $(this).attr('id');
            DatatableHelper.configSelectable(tableID, opt);
        }
	};	
    option.aoColumns = buildColumnsOption();
	return option;
}

Datatable.build = function(search_data, filter_data, tabid) {
    var table_id = 'My_DataTables_Table_' + tabid;
    var select = '#results' + tabid + '>form>table';
    $(select).attr('id', 'My_DataTables_Table_' + tabid);

    var option = buildOption(search_data, filter_data, tabid);

    $('#' + table_id).dataTable(option);
};


export {Datatable};