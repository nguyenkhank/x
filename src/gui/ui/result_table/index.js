import {ConfigData} from 'gui/components/config_data';
import {Render} from './render';
import {Datatable} from './datatable';

var ResultTable = {};

ResultTable.prepareTemplate = function () {
	Render.createTemplate();
}

ResultTable.Datatable = Datatable;

export {ResultTable};