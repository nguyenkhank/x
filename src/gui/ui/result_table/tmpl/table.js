var filterTemplate = `
   <div class="filter-datatable" tabid="{0}">
      <ul>
      <% _.each(filterCols ,function(col) { %>
         <% if(col.filterType == 'date') { %> 
            <li name="<%= col.group + '__' + col.name %>" data-type="date" >
                <%= col.headerText %>
            </li>
         <% } else if(col.filterType == 'text') { %>
            <li name="<%= col.group + '__' + col.name %>">
                <%= col.headerText %>
            </li>
         <% } else if(col.filterType == 'select') { %>
            <li name="<%= col.group + '__' + col.name %>">
               <%= col.headerText %>
               <ul>
                  <% _.each(col.selectValue, function(value) { %>
                     <li name="<%= value.id %>" ><%= value.text %></li>
                  <% }) %>
               </ul>
            </li>
         <% } %>
      <% }) %>
      </ul>
   </div>
`;

var actionTemplate = `
   <div data-control="datatable-action" class="dropdown pull-right">
      <button class="btn btn-sm btn-default dropdown-toggle" type="button" id="dropdownMenu{0}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
         <%= text('Actions') %> &nbsp;<span class="caret"></span>
      </button>
      <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
         <% _.each(actions ,function(action) { %>
            <li><a onclick="<%= action.fn %>" href="javascript:void(0);"> <%= action.title %> </a></li>
         <% }) %>
      </ul>
   </div>
`;

var lastAccessTemplate = `
   <div class="lastaccessed"  >
      <ul>
         <li>
            <span class="lastdocumentaccessed"><%= text('Last accessed') %>: <span class="document"></span></span>
         </li>
      </ul>
   </div>
`;



var TableTemplate = `<div role="tabpanel" class="tab-pane" id="results{0}">
   <div class="row">
      <div class="col-xs-4">
         ${filterTemplate}
      </div>
      <div class="col-xs-8">
         ${actionTemplate}
         ${lastAccessTemplate}
      </div>
   </div>
   <form class=" datatable_form" action="useradminitration/documentprocessing/whatever">
      <table class="table table-bordered table-hover table-striped">
         <thead>
            <tr>
               <% _.each(columns ,function(col) { %>
                  <th style="<%= StyleBuilder.BIU(col.headerBold, col.headerItalic, col.headerUnderline) %>">
                     <%= col.headerText %>
                  </th>
               <% }) %>
            </tr>
         </thead>
         <tbody></tbody>
      </table>
   </form>
</div>`;

export {TableTemplate};