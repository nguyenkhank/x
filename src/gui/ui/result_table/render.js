import {RenderFactory} from 'base/render_factory';
import {TableTemplate} from './tmpl/table';
import {TemplateData} from 'template/components/template_data';
import {ConfigData} from 'gui/components/config_data';

var Render = RenderFactory.create();
Render.createTemplate = function() {
	// get filter columns 
	var filterCols = TemplateData.Column.getFilters();
	var columns = _.sortBy(TemplateData.Column.getData(), 'order');
	
	// push more columns
	var customCols = ConfigData.getCustomColumns();
	_.each(customCols, function(customCol){
		columns.push({ headerText: customCol.title });
	});
	// end push more columns

	var renderData = {
		filterCols: filterCols,
		columns: columns,
		actions: [
			{title: 'Save to CSV', fn: 'exportCSV()'},
		],
	}
	var html = Render.buildTemplate(TableTemplate, renderData);
	$('#' + ConfigData.resultTableTemplateID()).html(html);
}


export {Render};