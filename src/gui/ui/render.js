import {RenderFactory} from 'base/render_factory';
import {BaseTemplate} from './tmpl/base';


var Render = RenderFactory.create();

Render.init = function(containerID){
	var html = Render.buildTemplate(BaseTemplate, {containerID: containerID});
	$('#' + containerID).html(html);
}

export {Render};