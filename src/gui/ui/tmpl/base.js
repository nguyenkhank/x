var BaseTemplate = `
<div class="row margin-bottom-10">
	<div class="col-xs-12">
        <form class="form-horizontal"  role="form" id="<%= containerID %>_Form"
        	skip-first-check="true" 
        ></form>
    </div>
</div>

<!-- RESULT TABS -->
<div class="row margin-bottom-10">
    <div class="col-xs-12" id="ajax-response">
		<ul id="results_tab" class="nav nav-tabs" role="tablist" data-control="results-tabs-nav"></ul>

		<!-- Tab panes -->
		<div class="tab-content" data-control="results-tabs-content">
			<div class="margin-bottom-10"> </div>
		</div>
    </div>
</div>

<script type="text/template" id="<%= containerID %>_ResultTemplate" ></script>
`;

export { BaseTemplate }