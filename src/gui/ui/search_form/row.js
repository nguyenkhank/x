import {Render} from './render';

import {Hooks} from 'gui/components/hooks';

var Row = {};

Row.render = function (rowInfo) {
	Render.appendRow(rowInfo);

	Hooks.SearchForm.Row.afterRender.run(rowInfo);
}


export {Row};