import {TemplateData} from 'template/components/template_data'
import {Structure} from './structure';

var SearchForm = {};

SearchForm.Structure = Structure;


SearchForm.render = function () {
	var confs = [{bundle: 'Section'},  {bundle: 'Row'}, {bundle: 'Control'}, ]; //  
	_.each(confs, function(conf){
		var items = TemplateData[conf.bundle].getData();
		var sortedItems = _.sortBy(items, function(item){
			return parseInt(item.order);
		});
		_.each(sortedItems, function(itemInfo){
			SearchForm.Structure[conf.bundle].render(itemInfo);
		})
	});
}


SearchForm.getListDefaultValue = function(){
	var controlsInfo = TemplateData.Control.getData();

	var bundle = {};
	_.each(controlsInfo, function(controlInfo){
		if(!controlInfo.defaultValue) {
			return;
		}
		var key = (controlInfo.group === '_custom') ? controlInfo.name : controlInfo.group + '__' + controlInfo.name;
		bundle[key] = controlInfo.defaultValue;
	});
	return bundle;
}

export {SearchForm};