import {Render} from './render';

var Control = {};

Control.render = function(controlInfo){
	// hook
	// var b4Run = CallbackHandler.runSequence(Hooks.SearchForm.Control.beforeRender, [controlInfo]);
 //    if(b4Run === false) {
 //        return false;
 //    } 
    // end hook 

	Render.appendControl(controlInfo); 	// APPEND TO DOM
    // initEvents(controlInfo);

    // hook
	// CallbackHandler.run(Hooks.SearchForm.Control.afterRender, [controlInfo]);
	// end hook 
    return controlInfo;
}

export {Control};