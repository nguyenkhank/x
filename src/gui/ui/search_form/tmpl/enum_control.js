var EnumTemplate = `
<div class="col-xs-<%= labelCols %>"  style="text-align: <%= labelAlign %>">
    <label class="control-label"><%= labelText %></label>
</div>
<div class="col-xs-<%= cols %>">
    <div class="input-group">
        <select data-control="search-criteria" <%= multiple ? 'multiple' : '' %> data-control-type="select" 
            data-width="100%" class="form-control additional-citeria" data-placeholder="" name="<%= group + '__' + name %>">
            <% _.each(dataList, function(opt){ %>
                <option value="<%= opt.id %>"><%= opt.text %></option>
            <% }); %>
        </select>
    </div>
</div>
`;
export {EnumTemplate};