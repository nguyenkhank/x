var SearchTypeTemplate = `
<div class="col-xs-offset-3 col-xs-12">
	<input class="form-control" data-control="search-criteria"
       data-control-type="searchType"
       type="hidden" name="<%= name %>"
       data-values="<%= _.pluck(options, 'value').join('|') %>"
       data-labels="<%= _.pluck(options, 'text').join('|') %>"
     />
</div>`;


export {SearchTypeTemplate};