var DateTimeRangeTemplate = `
<div class="col-xs-<%= labelCols %>"  style="text-align: <%= labelAlign %>">
    <label class="control-label"><%= labelText %></label>
</div>
<div class="col-xs-<%= cols %>">
    <div class="input-group date">
        <select data-control="search-criteria"  data-control-type="select" 
            data-width="100%" class="form-control additional-citeria" data-placeholder="" name="DateType">
            <% _.each(options, function(opt){ %>
                <option value="<%= opt.id %>"><%= opt.text %></option>
            <% }); %>
        </select>
    </div>
</div>`;


export {DateTimeRangeTemplate};