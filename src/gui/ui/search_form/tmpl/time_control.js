var TimeTemplate = `
<div class="col-xs-<%= labelCols %>"  style="text-align: <%= labelAlign %>">
    <label class="control-label"><%= labelText %></label>
</div>
<div class="col-xs-<%= cols %>">
    <div class="input-group date">
        <input data-control="search-criteria" data-control-type="timepicker"
                type="text" class="form-control input-sm" name="<%= name %>" placeholder="<%= name %>"/>
            <span class="input-group-addon input-sm"><i class="glyphicon glyphicon-time"></i></span>
    </div>
</div>`;


export {TimeTemplate};