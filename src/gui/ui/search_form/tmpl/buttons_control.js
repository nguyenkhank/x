var ButtonsTemplate = `
<div class="col-xs-offset-3 col-xs-<%= cols %>">
    <button type="button" data-control="btn-search"
            class="btn btn-default btn-sm submit_search"><%= text('Search') %></button>
    <button type="button" data-control="btn-reset"
            class="reset btn btn-default btn-sm"><%= text('Reset') %></button>
    <span class="btn-advanced-search" data-control="btn-toggle"> 
	    <span class="text-toggle"><%= text('Show') %></span>
	    <span class="text-toggle"><%= text('Hide') %></span> 
	    <%= text('Advanced Search') %>
   </span>
</div>
`;
export {ButtonsTemplate}