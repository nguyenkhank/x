var InfoTemplate = `
    <span class="fa fa-info-circle fa-fw fa-2x" data-control="search-tooltip"
          id="search3_<%= id %>"
          aria-hidden="true" data-toggle="tooltip" data-placement="bottom"
          title="<%= tooltip  %>"></span>
`;

export {InfoTemplate};