var SectionTemplate = `<div id="search3_<%= id %>" 
	data-control="search-<%= name %>" 
	<% if(name === 'advanced') { %> 
		class="menu hide"
	<% } %>
 ></div>`;

export {SectionTemplate};