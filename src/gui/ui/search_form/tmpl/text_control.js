var TextTemplate = `
    <div class="col-xs-<%= labelCols %>" style="text-align: <%= labelAlign %>">
        <label class="control-label"> 
            <%= labelText %>
        </label>
    </div>
    <div class="col-xs-<%= cols %>">
        <input type="text" data-control="search-criteria" data-control-type="tagsinput"
        	class="form-control input-sm" name="<%= (group == '_custom') ? name : group + '__' + name %>" />
    </div>`;


export {TextTemplate};