var linkAjax = "<%= base_url + 'eform/searchservice/dropdown_data?source=' + sourceGroup +  '&column=' + columnSelected + '&display=' + displayFormat %>";

var selectHtml = `
 <select data-control="search-criteria" 
    data-control-type="select" 
    data-width="100%" class="form-control additional-citeria" 
    data-allowclear="true"
    data-placeholder="" 
    name="<%= group + '__' + name %>"
    <%= multiple ? 'multiple' : '' %> 

    <% if(searchAjax) { %> 
        data-ajax-url="${linkAjax}"
    <% } else { %>
        data-init-url="${linkAjax}"
    <% } %>
></select>
`;

var SelectTemplate = `
<div class="col-xs-<%= labelCols %>"  style="text-align: <%= labelAlign %>">
    <label class="control-label"><%= labelText %></label>
</div>
<div class="col-xs-<%= cols %>">
    ${selectHtml}
</div>
`;
export {SelectTemplate}