import {ConfigData} from 'gui/components/config_data';
import {RenderFactory} from 'base/render_factory';

import {SectionTemplate} from './tmpl/section';
import {RowTemplate} from './tmpl/row';

import {ButtonsTemplate} from './tmpl/buttons_control';
import {DateTimeRangeTemplate} from './tmpl/datetimerange_control';
import {SearchByTemplate} from './tmpl/search_by_control';
import {InfoTemplate} from './tmpl/info_control';
import {TextTemplate} from './tmpl/text_control';
import {DateTemplate} from './tmpl/date_control';
import {TimeTemplate} from './tmpl/time_control';
import {EnumTemplate} from './tmpl/enum_control';
import {SelectTemplate} from './tmpl/select_control';
import {SearchTypeTemplate} from './tmpl/search_type_control';

var getControlTemplate = function(controlInfo){
	switch(controlInfo.display) {
		case 'datetimerange':
			return DateTimeRangeTemplate;
		case 'time':
			return TimeTemplate;
		case 'date':
			return DateTemplate;
		case 'buttons':
			return ButtonsTemplate;
		case 'info':
			return InfoTemplate;
		case 'tags':
		case 'text':
			return TextTemplate;
		case 'enum':
			return EnumTemplate;
		case 'searchby':
			return SearchByTemplate;
		case 'select':
			return SelectTemplate;
		case 'searchtype':
			return SearchTypeTemplate;

	}
	klog('NEED IMPLEMENT: ', controlInfo.display);

	return '<-- default -->';
}



var Render = RenderFactory.create();

Render.appendSection = function(sectionInfo){
	var html = Render.buildTemplate(SectionTemplate, sectionInfo);
	$('#' + ConfigData.formID() ).append(html);
}

Render.appendRow = function(rowInfo){
	var html = Render.buildTemplate(RowTemplate, rowInfo);
	$('#search3_' + rowInfo.sectionID ).append(html);
}

Render.appendControl = function(controlInfo) {
	var template = getControlTemplate(controlInfo);
	var html = Render.buildTemplate(template, controlInfo);


	var containerID = controlInfo.containerID || controlInfo.rowID;
	$('#search3_' + containerID).append(html);
}

export {Render};