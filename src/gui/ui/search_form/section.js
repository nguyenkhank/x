import {Render} from './render';

var Section = {};

Section.render = function (sectionInfo) {
	// klog('RENDER SECTION ' , sectionInfo);
	Render.appendSection(sectionInfo);
}
	
export {Section};