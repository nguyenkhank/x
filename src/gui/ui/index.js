import {TemplateData} from 'template/components/template_data';
import {ConfigData} from 'gui/components/config_data';
import {ResultTable} from './result_table';
import {SearchForm} from './search_form';

import {DataDefinition} from 'gui/components/data_defination';

var UI = {};

var initSearch2 = function(){

	var defaultValue = SearchForm.getListDefaultValue();

	klog('DEFAULT VALUE: ', defaultValue);

	$("#" + ConfigData.formID() ).search2({
        fnDataTableName: ResultTable.Datatable.build,
        fnValidate: function(criteria){
            var state = criteria.search_state; // basic or advanced
            var constraints = DataDefinition.schemaContraint(state);
            var result = validate(criteria, constraints);
            if(_.isEmpty(result)) {
                return { result: 'success', };
            } 
            
            var messages = _.map(result, function(error , fieldName){
                return {
                    field: fieldName,
                    order: 10,
                }
            })

            return {
                messages: messages,
                result: 'failure'
            }
        },
        initCriteria: defaultValue,
        hooks: {
            beforeAddTab: function (runtimeOption, search_data) {
            	// BUILD TEMPLATE
            	runtimeOption._resultsHTML = $('#' + ConfigData.resultTableTemplateID()).html();
            }
        }

        // hooks: {
        //     afterAddTab: function(){
        //         var documentID = B2BESinglePage.DataStore.get('DocumentID');
        //         if(documentID) {
        //             $('.lastdocumentaccessed .document').html(documentID);
        //         }
        //     }
        // },
        // firstTabName: langText.DocumentProcessing
    });
}

UI.setDocumentID = function(documentID){
    // klog('SET DOCUMENT ID', documentID, ConfigData.documentID())
    $('#' + ConfigData.documentID()).html(documentID);
}

UI.draw = function(data){
	TemplateData.setData(data);
	// draw
	SearchForm.render();
	ResultTable.prepareTemplate();

	// init search 2
	initSearch2();
    var documentID = ConfigData.getDocumentID();
    if(documentID) {
        UI.setDocumentID(documentID);
    }
}


export {UI};