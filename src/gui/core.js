import {Render} from './ui/render';
import {ConfigData} from 'gui/components/config_data';
import {Hooks} from 'gui/components/hooks';

import {InvoiceHandler} from './handler/invoice_handler';
import {FilterHandler} from './handler/filter_handler';
import {SearchTypeHandler} from './handler/search_type_handler';
import {AutoWithHandler} from './handler/auto_width_handler';

var Core = {};

Core.init = function (option) {
	if(!_.has(option, 'containerID')) {
		console.error('Option required `containerID`!');
		return;
	}

	FilterHandler.init(Hooks);
	SearchTypeHandler.init(Hooks);

	ConfigData.setUserOption(option);
	Render.init(option.containerID);

	AutoWithHandler.init(Hooks);

	if(_.has(option, 'mainGroup')) {
		switch(option.mainGroup) {
			case 'Invoice_Activity':
				InvoiceHandler.init(Hooks);
				break;
		}
	}
	// register hooks
}


	

export {Core};