import {TemplateData} from 'template/components/template_data';
import {dbDate} from 'base/helper/datetime_helper';


var FilterHandler = {};

var getSign = function(value){
	var res = null;
	_.each(['<', '>', '='], function(sign){
		if(value.startsWith(sign)) {
			res = sign;
			return false;
		}
	});
	return res;
}

var getValue = function(value) {
	var sign = getSign(value);
	return value.substring(sign.length);
}


var filterData = function(aoData, searchData, filterData, queryInf, sortInfo){
	if(!filterData) {
		return;
	}
	var value = filterData.value;

	var pieces = filterData.field.split('__'); // group __ name
	var info = TemplateData.Column.getByNameAndGroup(pieces[1] ,pieces[0]);

	if(info.filterType == 'date' && getSign(value) != null) {
		filterData.sign = getSign(value);
		filterData.value = dbDate(getValue(value));
	}

}

FilterHandler.init = function (Hooks) {
	Hooks.push('ResultTable.beforeRequestServer', filterData);
}

export {FilterHandler};