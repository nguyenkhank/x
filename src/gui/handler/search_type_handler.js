import {TemplateData} from 'template/components/template_data';


var SearchTypeHandler = {};
var searchTypeData = function(aoData, searchData, filterData, queryInf, sortInfo){
	if(!_.isObject(searchData)) {
		return;
	}
	if(!_.has(searchData, 'searchType')) {
		return;
	}

	var controlInfo = TemplateData.Control.get({group: '_custom', name: 'SearchType'});
	if(!controlInfo) {
		return;
	}

	var value = searchData.searchType;
	var option = _.findWhere(controlInfo.options, {value: value});

	var data = _.pick(option, ['value', 'group', 'field']);
	searchData.searchType = data;
}

SearchTypeHandler.init = function (Hooks) {
	Hooks.push('ResultTable.beforeRequestServer', searchTypeData);
}

export {SearchTypeHandler};