import {ConfigData} from 'gui/components/config_data';

var InvoiceHandler = {};

var REQUIRED_COLUMNS = [
	{
		group: 'Invoice_Activity', field: 'HardCopyB2BEInternalID',
	}, 
	{
		group: 'ActivityLock', field: 'Type',
	}, 
	{
		group: 'ActivityLock', field: 'TimeoutDateTime',
	}, 
	{
		group: 'UserLock', field: 'FirstName', through_group: 'ActivityLock',
	}, 
	{
		group: 'UserLock', field: 'LastName', through_group: 'ActivityLock',
	}, 
];


var beforeRequestServer = function (aoData, searchData, filterData, queryInf, sortInfo) {
	var moreGroups = [];
	_.each(REQUIRED_COLUMNS, function (criteria) {
		var colInfo = _.findWhere(queryInf.columns, criteria);
		if(!colInfo) { // check if not exist ... then insert
			queryInf.columns.push(criteria);
		}
		moreGroups.push(criteria.group);
	});

	queryInf.groups = queryInf.groups.concat(moreGroups);
	queryInf.groups = _.uniq(queryInf.groups);
}	

InvoiceHandler.init = function (Hooks) {
	Hooks.push('ResultTable.beforeRequestServer', beforeRequestServer);
	// REQUEST MORE DATA COLUMN

	// action column
	ConfigData.pushCustomColumn({
		title: '',
		width: '5%',
		render: function(data, column, full) {

			var pdfHtml = DOMHelper.pdfView(full.linkPDF);
			var lockHtml = '';
			if(!full.lockExpired && !!full.ActivityLock__Type) {
				var title = full.UserLock__FirstName + ' ' + full.UserLock__LastName;
				lockHtml = DOMHelper.lockView(full.ActivityLock__Type, title);
			}

			return "<div style='text-align:center; display: inline-flex'>" 
				+ pdfHtml + '&nbsp;'
				+ lockHtml
				+ "</div>";
		}
	});
	// BUILD MORE CUSTOM COLUMN
}




export {InvoiceHandler};