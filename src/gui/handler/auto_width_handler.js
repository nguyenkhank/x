import {TemplateData} from 'template/components/template_data';

var AutoWithHandler = {}
var afterRender = function(rowInfo){
	
	var id1 = `${rowInfo.id}_1`;
	var id2 = `${rowInfo.id}_2`;

	var str1 = `<div class="col-xs-6"><div class="form-group form-group-sm"><div class="row" id="search3_${id1}"></div></div></div>`;
	var str2 = `<div class="col-xs-6"><div class="form-group form-group-sm"><div class="row" id="search3_${id2}"></div></div></div>`;

	$('#search3_' + rowInfo.id).html(str1 + str2);
	var controls = TemplateData.Row.getControls(rowInfo);	

	// klog(rowInfo, controls)

	var hasOne = false;
	_.each(controls, function(controlInfo, index){
		// set rowID
		if(controlInfo.type == 'info') {
			controlInfo.containerID = id1;
			return;
		}
		if(hasOne) {
			controlInfo.containerID = id2;
		} else {
			controlInfo.containerID = id1;
		}
		hasOne = true;
		// end set rowID

		// set width for control & label
		controlInfo.labelCols = 3;
		switch(controlInfo.type) {
			case 'searchby':
			case 'date':
			case 'time': 
			case 'datetimerange':
				controlInfo.cols = 5;
				break;
			default: 
				controlInfo.cols = 8;
		}
	});
}


AutoWithHandler.init = function (Hooks) {
	Hooks.push('SearchForm.Row.afterRender', afterRender);
}

export {AutoWithHandler};