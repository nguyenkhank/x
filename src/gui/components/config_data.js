import {ConfigDataFactory} from 'base/config_data_factory';

var option = {
	documentID: null,
	serverHooks: null,
	//
	containerID: null,
	mainGroup: null,	
	// for columns
	customColumns: [], // custom columns

	// click 
	selectedMultile: true,
    onDoubleClick: null,
    affectedNodes: []
    // end click
}

var ConfigData = ConfigDataFactory.create(option);

ConfigData.setUserOption = function (userOption) {
	if(_.has(userOption, 'customColumns')) {
		option.customColumns = option.customColumns.concat(userOption.customColumns);
		delete userOption.customColumns;
	}
	_.extend(option, userOption);
}

ConfigData.getCustomColumns = function () {
	return option.customColumns;
}

ConfigData.getServerHooks = function(){
	return option.serverHooks;
}


ConfigData.getDoubleClick = function(){
	return _.pick(option, ['selectedMultile', 'onDoubleClick', 'affectedNodes']);
}



ConfigData.getDocumentID = function(){
	return option.documentID;
}

ConfigData.pushCustomColumn = function (colunmConfig) {
	if(!_.isArray(option.customColumns)) {
		option.customColumns = [];
	}
	option.customColumns.push(colunmConfig);
}

ConfigData.documentID = function(){
	return option.containerID + ' .lastdocumentaccessed .document';
}


ConfigData.formID = function(){
	return  option.containerID + '_Form';
}

ConfigData.resultTableTemplateID = function(){
	return option.containerID + '_ResultTemplate';
}


export {ConfigData};