import {ConfigData} from './config_data';
import {TemplateData} from 'template/components/template_data';

var DataDefinition = {};

var getFieldsFromResultTable = function(){
	var columns = TemplateData.Column.getData();
	columns = _.map(columns, function(column){
		// define column
		var col = {
			field: column.name,
			group: column.group,
		}
		if(_.has(column, 'through_group')) {
			col.through_group = column.through_group;
		}
		// end define
		return col;
	});
	return columns;
}

var getFieldsFromSearchForm = function(tableFields, search_data){
	var controls = TemplateData.Control.getData();
	klog(controls)
	var columns = [];
	_.each(controls, function(control){
		if(control.group == '_custom') { // ignore custom group
			return; 
		}
		var criteria = {name: control.name, group: control.group};
		var column = _.findWhere(tableFields,  criteria);
		if(!_.isEmpty(column)) { // already exists in list
			return;
		}

		var key = control.group + '__' + control.name;
		if(!_.has(search_data, key)) { // check key need to search
			return;
		}

		// define column
		var col = {
			field: control.name,
			group: control.group,
		}
		if(_.has(control, 'through_group')) {
			col.through_group = control.through_group;
		}
		// end define
		columns.push(col);
	});
	return columns;
}

// stateSection: basic or advanced
DataDefinition.schemaContraint = function(stateSection){
	var controls = TemplateData.Control.getData();

	var schema = {};
	_.each(controls, function(controlInfo){
		var sectionInfo = TemplateData.Control.getSection(controlInfo);
		// klog(sectionInfo, controlInfo)
		if(!sectionInfo || sectionInfo.name != stateSection) {
			return;
		}
		if(controlInfo.required) {
			if(controlInfo.group == '_custom') {
				schema[controlInfo.name]  = { presence : true };
			} // presence
		}
		// console.log(controlInfo, sectionInfo)
	});

	klog(schema)
	return schema;
}


// get uniq group & field of search form and result table
DataDefinition.queryInfo = function (search_data) {
	var tableFields = getFieldsFromResultTable();
	var tableGroups = _.pluck(tableFields, 'group');


	var formFields = getFieldsFromSearchForm(tableFields, search_data);
	var formGroups = _.pluck(formFields, 'group');
	
	
	var columns = tableFields.concat(formFields);
	var groups = _.uniq(tableGroups.concat(formGroups));
	return {
		columns: columns,
		groups: groups,
	}
}

DataDefinition.sortInfo = function(aoData){
	var opt = _.indexBy(aoData, 'name');
	// GET SORTED COLUMNS
	var sortInfo = [];
	var opt = _.indexBy(aoData, 'name');
	for (var i = 0; i < opt.iSortingCols.value; i++) {
		var fieldIndex = opt['iSortCol_' + i].value;
		var direction  = opt['sSortDir_' + i].value;
		var fieldName  = opt['mDataProp_' + fieldIndex].value; // 
		sortInfo.push({field: fieldName, direction: direction});
	}
	// END GET SORTED
	return sortInfo;
}


export {DataDefinition};