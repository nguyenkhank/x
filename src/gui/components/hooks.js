import {HookListFactory} from 'base/hook_list_factory';

var Hooks = {};

Hooks.SearchForm = {
	Row: {
		afterRender: HookListFactory.create(),
	},
}

Hooks.ResultTable = {
	beforeRequestServer: HookListFactory.create(),
}

Hooks.push = function (path, func) {
	var listHook = _.accessStr(Hooks, path);
	if(listHook == null) {
		console.warn('Not found path: ' + path + ' in Hooks!');
		return false;
	}
	listHook.push(func);
}

Hooks.register = function(customHooks) {
	if(!_.isObject(customHooks)) {
        return false;
    }
    _.each(customHooks, function(customHook, path){
    	Hooks.push(path, customHook);
    });
}



export {
	Hooks
}