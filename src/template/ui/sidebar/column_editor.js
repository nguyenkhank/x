import {Hooks} from 'template/components/hooks';
import {Sidebar} from 'template/ui/sidebar';
import {ResultTable} from 'template/ui/result_table';

var  ColumnEditor = {};

var $Form = null;

var currentColumn = null;
ColumnEditor.init = function(){
    // render html 
    // init b2be form
    var attrOption = {
        'name': {
            type: B2BEForm.OBJECT_TYPE.TEXT,
            disabled: true,
        },
        'group': {
            type: B2BEForm.OBJECT_TYPE.TEXT,
            disabled: true
        },
        hide: {
            type: B2BEForm.OBJECT_TYPE.CHECKBOX,
        },
        // for filter feature
        filter: {
            type: B2BEForm.OBJECT_TYPE.CHECKBOX,
            events: {
                change: function(e) {
                    var value = e.moreInfo.value; 
                    if(value) {
                        $('#columnEditor .filterTypeWrapper').show();
                    } else {
                        $('#columnEditor .filterTypeWrapper').hide();
                    }
                }
            }
        },
        filterType: {
            type: B2BEForm.OBJECT_TYPE.RADIO, 
            events: {
                change: function(e){
                    var value = e.moreInfo.value; 
                    if(value === 'select') {
                        $('#columnEditor .selectValueWrapper').show();
                    } else {
                        $('#columnEditor .selectValueWrapper').hide();
                    }
                }
            }
        },
        selectValue: {
            type: B2BEForm.OBJECT_TYPE.CRUD,
            columns: {
                id: {
                    label: text('Value'), 
                },
                text: {
                    label: text('Display'), 
                }
            }
        },
        // end filter feature

        sort: {
            type: B2BEForm.OBJECT_TYPE.CHECKBOX,
        },
        // header
        headerBold: {
            type: B2BEForm.OBJECT_TYPE.CHECKBOX,
        },
        headerItalic: {
            type: B2BEForm.OBJECT_TYPE.CHECKBOX,
        },
        headerUnderline: {
            type: B2BEForm.OBJECT_TYPE.CHECKBOX,
        },
        headerText: {
            type: B2BEForm.OBJECT_TYPE.TEXT,
        },
        // end header

        dataFormat: {
            type: B2BEForm.OBJECT_TYPE.SELECT2,
            style: {width: '100%'},
            optionList: []
        },
        align: {
            type: B2BEForm.OBJECT_TYPE.SELECT2,
            style: {width: '100%'},
            optionList: [
                {id: 'left', text: 'Left'},
                {id: 'right', text: 'Right'},
            ] 
        },
    };

    B2BEForm.init('#columnEditor', attrOption).done(function($form){
        $Form = $form;
    });

    $('#columnEditor .applyEditor').click(function(){
        var values = $Form.getValues();
        _.extend(currentColumn, values);

        ResultTable.Structure.refreshColumn(currentColumn)
    })

    $('#columnEditor .closeEditor').click(function(){
        var b4Run = Hooks.ColumnEditor.beforeClose.runSequence(currentColumn);
        if(b4Run === false) {
            return false;
        } 

        Sidebar.showPanel('resultSelectorPanel');
        // hook
        Hooks.ColumnEditor.afterClose.run(currentColumn);
        // end hook 
    })
}

ColumnEditor.enable = function (columnInfo) {
    var b4Run = Hooks.ColumnEditor.beforeActive.runSequence(columnInfo);
    if(b4Run === false) {
        return false;
    } 

    currentColumn = columnInfo;
    Sidebar.showPanel('columnEditor');

    // update gui
    $('#columnEditor .filterTypeWrapper').hide();
    $('#columnEditor .selectValueWrapper').hide();

    if(columnInfo.filter) {
        $('#columnEditor .filterTypeWrapper').show();
        if(columnInfo.filterType === 'select') {
            $('#columnEditor .selectValueWrapper').show();
        }
    } 
    // end update gui

    $Form.setValues(columnInfo);
    // hook
    Hooks.ColumnEditor.afterActive.run(columnInfo, $Form);
    // end hook
}

export {
    ColumnEditor
}    

