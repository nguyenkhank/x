import {GroupTypeTemplate} from './common/group_type';

var groupTypeStr = GroupTypeTemplate.get({
	group: true,
	field: true,
	text: true,
	value: true,
});

var SearchTypeTemplate = `<h4 class="group-fields-title"><%= text('Search Type') %></h4>
<script id="searchTypeTemlate" type="text/template">
	${groupTypeStr}
</script>
<div id="searchTypeOptionEditor"></div>
`;

export {
    SearchTypeTemplate
}