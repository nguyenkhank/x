import {GroupTypeTemplate} from './common/group_type';

var groupTypeStr = GroupTypeTemplate.get({
	group: true,
	field: true,
	text: true,
	value: false,
});


var SearchByTemplate = `<h4 class="group-fields-title"><%= text('Search By') %></h4>
<script id="searchByTemlate" type="text/template">
    ${groupTypeStr}
</script>
<div id="searchByOptionEditor"></div>
`;

export {
    SearchByTemplate
}