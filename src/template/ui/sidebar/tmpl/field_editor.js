
var generalTemplate = `
<h4 class="group-fields-title"><%= text('General') %></h4>
<div class="row">
    <div class="col-xs-6 form-group">
        <label><%= text('Control Name') %>:  </label>
        <input class="form-control input-sm" name="name" disabled="disabled">
    </div>
    <div class="col-xs-6 form-group">
        <label><%= text('Group') %>:  </label>
         <input class="form-control input-sm" name="group" disabled="disabled">
    </div>
</div>
<!-- 
<div class="row">
    <div class="col-xs-6 form-group form-group-sm">
        <label><%= text('Width 12 parts') %>:  </label>
        <select class="form-control input-sm" name="cols"></select>
    </div>
    <div class="col-xs-6 form-group form-group-sm">
        <label><%= text('Align') %>:  </label>
        <select class="form-control input-sm" name="align"></select>
    </div>
</div>
-->
`;

var multiSelectTemplate = `
<div class="multipleWrapper" style="display: none;">
    <div class="row">
        <div class="col-xs-6 form-group form-group-sm">
            <label><%= text('Multiple select') %> <input type="checkbox" name="multiple" /> </label>
        </div>
    </div>
</div>
`;

var datasourceTemplate = `
<div class="datasourceWrapper" style="display: none;">
    <h4 class="group-fields-title"><%= text('Data Source') %></h4>
    <div class="row">
        <div class="col-xs-6 form-group  form-group-sm">
            <label><%= text('Source') %>:  </label>
            <input class="form-control" name="sourceGroup" />
        </div>
        <div class="col-xs-6 form-group  form-group-sm">
            <label><%= text('Column Selected') %>: </label>
            <input class="form-control" name="columnSelected" />
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 form-group">
            <label><%= text('Display Format') %>:  </label>
            <input class="form-control input-sm"  name="displayFormat"  />
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 form-group">
            <label><%= text('Search Ajax') %>:  <input type="checkbox" name="searchAjax" /> </label>
        </div>
    </div>
</div>
`;

var defaultValueTemplate = `
<div class="defaultValueWrapper">
    <h4 class="group-fields-title"><%= text('Default Value') %></h4>
    <div class="row">
        <div class="col-xs-12 form-group">
            <label><%= text('Default Value') %>:  </label>
            <input name="defaultValue" class="form-control input-sm" />
        </div>
    </div>
</div>
`;

var labelTemplate = `
<div class="labelWrapper">
    <h4 class="group-fields-title"><%= text('Label') %></h4>
    <div class="row">
        <div class="col-xs-12 form-group">
            <label> <%= text('Text') %>:  </label>
            <input class="form-control input-sm"  name="labelText"  />
        </div>
    </div>
    <!-- 
    <div class="row">
        <div class="col-xs-6 form-group form-group-sm">
            <label> <%= text('Width 12 parts') %>:  </label>
            <select class="form-control" name="labelCols"></select>
        </div>
        <div class="col-xs-6 form-group form-group-sm">
            <label> <%= text('Align') %>: </label>
            <select class="form-control" name="labelAlign"></select>
        </div>
    </div>
    -->
</div>
`;

var dataListTemplate = `
<div class="datalistWrapper" style="display: none;">
    <h4 class="group-fields-title"><%= text('Data List') %></h4>
    <div class="row">
        <div class="col-xs-12 form-group">
            <div name="dataList"></div>
        </div>
    </div>
</div>
`;

/***********************************************
************************************************
**************   MAIN TEMPLATE
************************************************
************************************************/
var FieldEditorTemplate = `
<div class="margin-bottom-10">
	<span class="route-title">Field</span> 
	<span class="pull-right">
		<button class="applyEditor btn btn-default btn-sm">Apply</button>
		<button class="closeEditor btn btn-default btn-sm">Close</button>
	</span> 
</div>

${generalTemplate}

<div class="row">
    <div class="col-xs-6 form-group form-group-sm">
        <label><%= text('Display') %>:  </label>
        <select class="form-control input-sm" name="display"></select>
    </div>

    <div class="col-xs-6 form-group form-group-sm"> <br />
        <label><%= text('Required') %> <input type="checkbox" name="required" />
        </label>
    </div>
</div>

${multiSelectTemplate}

<!-- START -->
${datasourceTemplate}
<!-- END -->

<!-- START -->
${dataListTemplate}
<!-- END -->

<!-- START -->
${defaultValueTemplate}
<!-- END -->

<!-- START -->
${labelTemplate}
<!-- END -->
<div class="customWrapper"></div>
`

export {
	FieldEditorTemplate
}