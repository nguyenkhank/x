var CustomInfoControlTemplate = `<h4 class="group-fields-title"><%= text('Tooltip') %></h4>
<div  class="row">
    <div class="col-xs-12 form-group form-group-sm">
        <label><%= text('Text') %>:  </label>
        <textarea name="tooltip" class="form-control"></textarea>
    </div>
</div>`;

export {
    CustomInfoControlTemplate
}