var ColumnEditorTemplate = `<div class="margin-bottom-10">
	<span class="route-title"><%= text('Column') %></span> 
	<span class="pull-right">
		<button class="applyEditor btn btn-default btn-sm"><%= text('Apply') %></button>
		<button class="closeEditor btn btn-default btn-sm"><%= text('Close') %></button>
	</span> 
</div>


<h4 class="group-fields-title"><%= text('General') %></h4>
<div class="row">
    <div class="col-xs-6 form-group">
        <label> <%= text('Control Name') %>:  </label>
        <input class="form-control input-sm" name="name" disabled="disabled">
    </div>
    <div class="col-xs-6 form-group">
        <label> <%= text('Group') %>:  </label>
         <input class="form-control input-sm" name="group" disabled="disabled">
    </div>
</div>

<div class="row">
    <div class="col-xs-6 form-group form-group-sm">
        <label> <%= text('Align') %>:  </label>
        <select name="align"  class="form-control input-sm"></select>
    </div>
    <div class="col-xs-6 form-group form-group-sm">
        <label> <%= text('Width') %>:  </label>
         <input class="form-control input-sm" value="auto" name="width">
    </div>
</div>

<h4 class="group-fields-title"><%= text('Data Format') %></h4>
<div class="row">
    <div class="col-xs-6 form-group form-group-sm">
    	<label> <%= text('Data Format') %>:  </label>
        <select name="dataFormat" class="form-control input-sm"></select>
    </div>

    <div class="col-xs-6 form-group">
    	<br/>
    	<label> Tenant Date <input type="checkbox" name="readonly" type="checkbox" /></label> 
    	<!-- <label> Tenant DateTime <input type="checkbox" name="readonly" type="checkbox" /></label> -->
    </div>
</div>

<h4 class="group-fields-title"><%= text('Action') %></h4>
<div class="row">
	<div class="col-xs-12 form-group">
        <label> <%= text('Hide') %> <input type="checkbox" name="hide"  /> </label> 
        | <label> <%= text('Filter') %> <input type="checkbox" name="filter" /> </label> 
        | <label> <%= text('Sort') %> <input type="checkbox" name="sort" /> </label> 
    </div>
</div>

<div class="filterTypeWrapper">
    <div class="row">
        <div class="col-xs-12 form-group">
            Filter Type: 
            <label>
                <input type="radio" value="text" name="filterType" /> <%= text('Text') %> 
            </label>
            <label>
                <input type="radio" value="date" name="filterType" /> <%= text('Date') %> 
            </label>
            <label>
                <input type="radio" value="select" name="filterType" /> <%= text('Select') %> 
            </label>
        </div>
    </div>
</div>

<div class="selectValueWrapper" style="display: none;">
    <div class="row">
        <div class="col-xs-12 form-group">
            <div name="selectValue"></div>
        </div>
    </div>
</div>



<h4 class="group-fields-title"><%= text('Header Format') %></h4>
<div class="row">
	<div class="col-xs-12 form-group">
        <label> <%= text('Title') %>:  </label>
        <input class="form-control input-sm" name="headerText" />
    </div>
	<div class="col-xs-12 form-group">
        <label> <%= text('Bold') %> <input type="checkbox" name="headerBold"> </label> | 
        <label> <%= text('Italic') %> <input type="checkbox" name="headerItalic"> </label> |
        <label> <%= text('Underline') %> <input type="checkbox" name="headerUnderline"> </label> 
    </div>
</div>`;


export {
	ColumnEditorTemplate,
}