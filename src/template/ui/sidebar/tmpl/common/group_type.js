var valueWrapper = `<div class="row">
    <div class="form-group form-group-sm">
        <label class="control-label col-xs-3"><%= text('Value') %></label>
        <div class="col-xs-9">
            <input class="form-control" name="value" />
        </div>
    </div>
</div>`;

var textWrapper = `<div class="row">
    <div class="form-group form-group-sm">
        <label class="control-label col-xs-3"><%= text('Text') %></label>
        <div class="col-xs-9">
            <input class="form-control" name="text" />
        </div>
    </div>
</div>`;

var fieldWrapper = `<div class="row">
    <div class="form-group form-group-sm">
        <label class="col-xs-3 control-label"><%= text('Field') %></label>
        <div class="col-xs-9">
            <select class="form-control" name="field"></select>
        </div>
    </div>
</div>`;

var groupWrapper = `<div class="row">
    <div class="form-group form-group-sm">
        <label class="col-xs-3 control-label"><%= text('Group') %></label>
        <div class="col-xs-9">
            <select class="form-control" name="group"></select>
        </div>
    </div>
</div>`;



var groupTypeTemplate = `
<div class="row">
    <br />
    <div class="col-xs-11 form-horizontal">
    	{{XXX}}
    </div>

    <div style="position: relative; top: -15px; right: 11px;">
        <label class="control-label">
            <i class="hand-cursor add fa fa-plus-circle" style="color: green"></i>
            <i class="hand-cursor remove fa fa-minus-circle" style="color: red"></i>
        </label>
    </div>
   
    <div class="clearfix"></div>
</div>

<div style="border-top: 1px solid #ddd; margin: 5px -5px;"></div>
`;


var GroupTypeTemplate = {};

GroupTypeTemplate.get = function(conf){
	var str = '';
	if(conf.group) {
		str += groupWrapper;
	}
	if(conf.field) {
		str += fieldWrapper;
	}
	if(conf.text) {
		str += textWrapper;
	}
	if(conf.value) {
		str += valueWrapper;
	}

	return groupTypeTemplate.replace('{{XXX}}', str);
}

export {GroupTypeTemplate};