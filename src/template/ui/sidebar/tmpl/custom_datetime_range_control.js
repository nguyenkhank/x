var DateTimeRangeTemplate = `<h4 class="group-fields-title"><%= text('Date Types') %></h4>
<script id="dateRangeTemlate" type="text/template">
<div class="row">

    <div class="col-xs-10">
        <div class="row">
            <div class="form-group form-group-sm">
                
                <div class="col-xs-12">
                	<label class="control-label"><%= text('Fields') %></label>
                    <select class="form-control" name="id"></select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group form-group-sm">
               
                <div class="col-xs-12">
                	 <label class="control-label"><%= text('Text') %></label>
                    <input class="form-control" name="text" />
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-1">
        <label class="control-label">
            <i class="hand-cursor add fa fa-plus-circle" style="color: green"></i>
            <i class="hand-cursor remove fa fa-minus-circle" style="color: red"></i>
        </label>
    </div>
   
    <div class="clearfix"></div>
</div>

<div style="border-top: 1px solid #ddd; margin: 5px -5px;"></div>
</script>

<div id="dateRangeOptionEditor"></div>
`;

export {
    DateTimeRangeTemplate
}