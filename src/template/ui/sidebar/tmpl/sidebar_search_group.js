var SearchGroupTemplate = `<h4 class="group-fields-title" ><%= Title %></h4>
<div data-fieldgroup="<%= GroupName %>" class="groupSelector list-group" style="height: 0px">
	<% _.each( Fields, function(field){ %>
		<a id="<%= field.GroupName %>-<%= field.FieldName %>-<%= Panel %>" 
			data-fieldname="<%= field.FieldName %>" 
			title="<%= field.FieldName %>" 
			href="javascript:void(0)" class="fieldSelector list-group-item">
	        	<i class="fa fa-<%= PageHTMLBuilder.fieldIcon(field.Type) %>"></i> <%= field.Title %>
	    </a>
	<% }) %>
</div>`;


export {
	SearchGroupTemplate
}