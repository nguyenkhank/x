import {Sidebar} from 'template/ui/sidebar';
import {ResultTable} from 'template/ui/result_table';
import {SearchForm} from 'template/ui/search_form';


var  FieldSelector = {};

var lastHeight = null;
var updateHeight = function(){
	var offsetTop = 60 ; // $('#sidebarTool').offset().top;
    var offsetBot = 60;
	var currentHeight = $(window).height();

	$('#sidebarTool').css('height', $(window).height() - offsetTop);

	$('.searchFieldsSelector, .resultColumnsSelector').css('max-height', currentHeight - offsetTop - offsetBot);


    var h = currentHeight - $('.searchFieldsSelector .group-fields-title').length * 32 - offsetTop - offsetBot; 
    $('.searchFieldsSelector .groupSelector').css('height', h);

    var h = currentHeight - $('.resultColumnsSelector .group-fields-title').length * 32 - offsetTop - offsetBot; 
    $('.resultColumnsSelector .groupSelector').css('height', h);


    // $('#fieldEditor').css('max-height', currentHeight - offsetTop);
    // $('#columnEditor').css('max-height', currentHeight - offsetTop);
}

FieldSelector.init = function () {
   	// set ui behavior
	$('.searchFieldsSelector, .resultColumnsSelector').accordion({
	    active: true,
	    collapsible: true,
	    heightStyle: "fill",
	}); 

	// INSERT CONTROL
    $('.fieldSelector', $(".searchFieldsSelector")).draggable({
        appendTo: 'body',
        containment: 'window',
        scroll: false,
        helper: 'clone',
        revert: true,
        cancel: ".list-group-item-success",
    });

    // INSERT COLUMNS
    $('.columnSelector', $(".resultColumnsSelector")).draggable({
        appendTo: 'body',
        containment: 'window',
        scroll: false,
        helper: 'clone',
        revert: true,
        cancel: ".list-group-item-success",
    });

    // REMOVE CONTROL
	$('.searchFieldsSelector').droppable({
        accept: ".control-wrapper",
        drop: function (event, ui){
            var $ele = ui.draggable;
            var controlID = $ele.attr('id');
            if(_.isEmpty(controlID)) {
                return;
            }
          	var controlInfo = SearchForm.Structure.Control.remove(controlID);
          	Sidebar.enableControl('Search', controlInfo.group, controlInfo.name);
        }
    });

    // REMOVE CONTROL
	$('.resultColumnsSelector').droppable({
        accept: ".column-item",
        drop: function (event, ui){
            var $ele = ui.draggable;
            var controlID = $ele.attr('id');
            klog(controlID)
            if(_.isEmpty(controlID)) {
                return;
            }
          	var columnInfo = ResultTable.Structure.removeColumn(controlID);
          	Sidebar.enableControl('Result', columnInfo.group, columnInfo.name);
        }
    });


    updateHeight();

	// events
	$(window).resize(function() {
        updateHeight();
    });
}

FieldSelector.disableControl = function(panel, group, name){
	var fieldID = group + '-' + name + '-'  + panel;
	$('#' + fieldID).addClass('list-group-item-success');
}

FieldSelector.enableControl = function(panel, group, name){
	var fieldID = group + '-' + name + '-'  + panel;
	$('#' + fieldID).removeClass('list-group-item-success');
}

export {
    FieldSelector
}    

