import {SearchForm} from '../search_form';
import {ResultTable} from '../result_table';

import {Render} from './render';
import {ColumnEditor} from './column_editor';
import {FieldEditor} from './field_editor';
import {FieldSelector} from './field_selector';

var Sidebar = {};
Sidebar.Render = Render;

Sidebar.disableControl = function(panel, group, name){
	FieldSelector.disableControl(panel, group, name);
}

Sidebar.enableControl = function(panel, group, name){
	FieldSelector.enableControl(panel, group, name);
}

Sidebar.init = function(){
	Render.init();
	FieldSelector.init();
	ColumnEditor.init();
	FieldEditor.init();

	$('#showResultSelector').click(function(){
		Sidebar.showPanel('resultSelectorPanel');
	});
}

Sidebar.ColumnEditor = ColumnEditor;

Sidebar.FieldEditor = FieldEditor;

Sidebar.showPanel = function(panelID){
	if(!panelID.startsWith('#')) {
		panelID = '#' + panelID;
	}
	$('#sidebarTool>.sidebarPanel').hide();
	$(panelID).show();
}

export { Sidebar };