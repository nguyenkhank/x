import {ConfigData} from  'template/components/config_data';

import {SearchGroupTemplate} from './tmpl/sidebar_search_group';
import {ResultGroupTemplate} from './tmpl/sidebar_result_group';

import {ColumnEditorTemplate} from './tmpl/column_editor';
import {FieldEditorTemplate} from './tmpl/field_editor';

import {CustomInfoControlTemplate} from './tmpl/custom_info_control';
import {DateTimeRangeTemplate} from './tmpl/custom_datetime_range_control';
import {SearchByTemplate} from './tmpl/custom_searchby_control';

import {SearchTypeTemplate} from './tmpl/custom_search_type_control';


import {RenderFactory} from 'base/render_factory';

var Render = RenderFactory.create();

Render.init = function(){
	// render base sidebar
	var html = Render.buildTemplate(ColumnEditorTemplate, {});
	$('#columnEditor').html(html);

	var html = Render.buildTemplate(FieldEditorTemplate, {});
	$('#fieldEditor').html(html);
	// end render base sidebar

	// render item of list group
	var dataGroupSidebar = ConfigData.dataSidebarSearch();
	_.each(dataGroupSidebar, function(groupData){
		var html = Render.buildTemplate(SearchGroupTemplate, groupData);
		$('#searchSelectorPanel .searchFieldsSelector').append(html);
	});

	var dataGroupSidebar = ConfigData.dataSidebarResult();
	_.each(dataGroupSidebar, function(groupData){
		var html = Render.buildTemplate(ResultGroupTemplate, groupData);
		$('#resultSelectorPanel .resultColumnsSelector').append(html);
	});
}

Render.customFieldEditor = function(display){
	var $el = $('#fieldEditor');

	switch(display) {
		case 'info':
			$el.find('.labelWrapper').hide();
			var html = Render.buildTemplate(CustomInfoControlTemplate, {});
			break;
		case 'datetimerange':
			var html = Render.buildTemplate(DateTimeRangeTemplate, {});
			break;
		case 'searchby': 
			var html = Render.buildTemplate(SearchByTemplate, {});
			break;
		case 'searchtype':
			$el.find('.labelWrapper').hide();
			var html = Render.buildTemplate(SearchTypeTemplate, {});
	}

	$el.find('.customWrapper').html(html);
	$el.find('.customWrapper').show();
}



export {
	Render
}