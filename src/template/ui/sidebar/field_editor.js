import {Sidebar} from 'template/ui/sidebar';
import {SearchForm} from 'template/ui/search_form';
import {Hooks} from 'template/components/hooks';

var FieldEditor = {};
var currentControl = null;
var $Form = null;

var toogleGUIType = function(type){
	var $el = $('#fieldEditor');

	$el.find('.datasourceWrapper').hide();
	$el.find('.datalistWrapper').hide();
    $el.find('.customWrapper').hide();

    $el.find('.multipleWrapper').hide();
    $el.find('.labelWrapper').show();

	switch(type) {
		case 'enum':
			$el.find('.datalistWrapper').show();
            $el.find('.multipleWrapper').show();
			break;
		case 'select':
			$el.find('.datasourceWrapper').show();
			break;
	}
}

var colsVals = _.range(1, 13);
var controlAlignVals = ['right', 'left'];

var controlOptions = {
    name: {
        type: B2BEForm.OBJECT_TYPE.TEXT,
        disabled: true,
    },
    group: {
        type: B2BEForm.OBJECT_TYPE.TEXT,
        disabled: true
    },
    // cols: {
    //     type: B2BEForm.OBJECT_TYPE.SELECT2,
    //     style: {width: '100%'},
    //     optionList: colsVals,
    // },
    // align: {
    //     type: B2BEForm.OBJECT_TYPE.SELECT2,
    //     style: {width: '100%'},
    //     optionList: controlAlignVals,
    // },

    display: {
        type: B2BEForm.OBJECT_TYPE.SELECT2,
        style: {width: '100%'},
        optionList: [],
        events: {
            change: function(e){
                var value = e.moreInfo.value;
                toogleGUIType(value);
            }
        }
    },

    multiple: {
        type: B2BEForm.OBJECT_TYPE.CHECKBOX,
    },
    // default value
    defaultValue: {
        type: B2BEForm.OBJECT_TYPE.TEXT,
    },
    // end 

    /// label
    // labelCols: {
    //     type: B2BEForm.OBJECT_TYPE.SELECT2,
    //     style: {width: '100%'},
    //     optionList: colsVals,
    // },
    // labelAlign: {
    //     type: B2BEForm.OBJECT_TYPE.SELECT2,
    //     style: {width: '100%'},
    //     optionList: controlAlignVals,
    //     defaultValue: 'left'
    // }, 
    labelText: {
        type: B2BEForm.OBJECT_TYPE.TEXT
    },
    // end label

    // data List, enum
    dataList: {
        type: B2BEForm.OBJECT_TYPE.CRUD,
        columns: {
            id: {
                label: text('Value'), 
            },
            text: {
                label: text('Display'), 
            }
        }
    },
    // end

    // dropdown
    sourceGroup: {
        type: B2BEForm.OBJECT_TYPE.TEXT,
        disabled: true
    },
    columnSelected: {
        type: B2BEForm.OBJECT_TYPE.TEXT,
        disabled: true
    },
    displayFormat: {
        type: B2BEForm.OBJECT_TYPE.TEXT,
    },
    searchAjax: {
        type: B2BEForm.OBJECT_TYPE.CHECKBOX,
    },
    // end dropdown

    // validate
    required: {
        type: B2BEForm.OBJECT_TYPE.CHECKBOX,
        defaultValue: false,
    }
};

FieldEditor.init = function(){
    B2BEForm.init('#fieldEditor', controlOptions).done(function($form){
        $Form = $form;
    });


    $('#fieldEditor .closeEditor').click(function(){
        Sidebar.showPanel('searchSelectorPanel');
        // hook
        Hooks.FieldEditor.afterClose.run(currentControl);
        // end hook
    })

    $('#fieldEditor .applyEditor').click(function(){
    	var values = $Form.getValues();
        // hook
        var b4Run = Hooks.FieldEditor.beforeApply.runSequence(currentControl, values);
        if(b4Run === false) {
            return false;
        } 
        // end hook

        _.extend(currentControl, values);
        SearchForm.Structure.Control.refresh(currentControl);

        // hook
        Hooks.FieldEditor.afterApply.run(currentControl, values);
        // end hook

    })
}

FieldEditor.enable = function (controlInfo) {
    var b4Run = Hooks.FieldEditor.beforeActive.runSequence(controlInfo);
    if(b4Run === false) {
        return false;
    } 

    currentControl = controlInfo;
    Sidebar.showPanel('fieldEditor');

    $Form.setValues(controlInfo);
    // hook
    Hooks.FieldEditor.afterActive.run(controlInfo, $Form);
    // end hook
}


export {
    FieldEditor
}    

