var BaseTemplate = `<div class="row">
<div class="col-md-9">
	<div id="searchForm">
		<h3 class="page-title">
			<i class="fa fa-search"></i> <%= text('Search Form') %>
			<span class="pull-right">
				<i class="fa fa-cog"></i>
				<i id="showSearchSelector" class="fa fa-caret-square-o-right"></i>
				<i data-target="#searchFormWrapper" data-toggle="collapse" class="fa fa-chevron-down"></i>
			</span>
		</h3>
		<div id="searchFormWrapper" class="collapse in"></div>  <!-- CONTENT -->
	</div>	 <!-- FORM SEARCH WRAPPER -->

	<h3 class="page-title">
		<i class="fa fa-th"></i> Result Table
		<span class="pull-right"><i id="showResultSelector" class="fa fa-caret-square-o-right"></i></span>
	</h3>
	<div>
		<div class="column-wrapper" id="resultColumnWrapper">
		</div>
		<div class="clearfix"></div>
	</div><!-- RESULT SEARCH -->
</div>
<div class="col-md-3">
	<div id="sidebarTool">
    	<div id="searchSelectorPanel" class="sidebarPanel " style="top: 0; width: 310px;  display: none;" data-spy="affix" data-offset-top="400" >
            <div class="margin-bottom-10"><span class="route-title">Search Form Fields</span></div>
            <div class="searchFieldsSelector panel panel-default">
            	<!-- CONTENT -->
            </div>
        </div> <!-- searchSelectorPanel -->

        <div id="resultSelectorPanel" class="sidebarPanel" style="top: 0; width: 310px; display: none;" data-spy="affix" data-offset-top="400" >
            <div class="margin-bottom-10"><span class="route-title">Result Form Fields</span></div>
            <div class="resultColumnsSelector panel panel-default">
            	<!-- CONTENT -->
            </div>
        </div> <!-- searchSelectorPanel -->


        <div id="fieldEditor" class="sidebarPanel" style="top: 0; width: 310px; display: none;" data-spy="affix" data-offset-top="400" >
        </div> <!-- searchSelectorPanel -->


        <div id="columnEditor" class="sidebarPanel" style="top: 0; width: 310px; display: none;" data-spy="affix" data-offset-top="400" >
        </div> <!-- searchSelectorPanel -->
	</div>
</div>
</div>`;


export { BaseTemplate }