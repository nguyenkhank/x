import {TemplateData} from 'template/components/template_data';
import {Hooks} from 'template/components/hooks';

import {Structure} from './structure';
import {Render} from './render';

var Section = {};

var initEvents = function(sectionInfo){
	// add row event
	$('#' + sectionInfo.id + ' .addRow').click(function(){
		Section.addRow(sectionInfo);
	})

	// sort row event
	$('#sec-body-' + sectionInfo.id).sortable({
        cursor: "move",
        delay: 200,
        update: function( event, ui ) {
        	Section.resetRowOrder(sectionInfo);
        }
    }).disableSelection();
	// remove row

	// delegate event remove row
    var bodyID = '#sec-body-' + sectionInfo.id;
	$(bodyID).on('click', '.removeRow', function(e){
		var $row = $(e.target).parents('.row-wrapper');
        var rowID = $row.attr('id');
        Structure.Row.remove(rowID);
	});	
	// END

    $(bodyID).on('shown.bs.collapse', function (e) {
        $('#' + sectionInfo.id).find('.field-arrow').addClass('fa-chevron-down').removeClass('fa-chevron-up');
    });

    $(bodyID).on('hidden.bs.collapse', function () {
        $('#' + sectionInfo.id).find('.field-arrow').addClass('fa-chevron-up').removeClass('fa-chevron-down');
    });
}

Section.render = function(sectionInfo){
    Render.appendSection(sectionInfo); // APPEND TO DOM
    initEvents(sectionInfo);
    // hook
    Hooks.SearchForm.Section.afterRender.run(sectionInfo);
    // end hook 
}

/// SECTION 
Section.register = function (basicInfo) {
	var sectionInfo = TemplateData.Section.register(basicInfo);
    Section.render(sectionInfo);

    // hook
    Hooks.SearchForm.Section.afterRegister.run(sectionInfo);
    // end hook 
	return sectionInfo;
}

Section.traverseRowDOM = function(secInfo, callback){
	var sectionInfo = TemplateData.Section.get(secInfo);
	var rowDOMs =  $('#sec-body-' + sectionInfo.id).find('.row-wrapper');
    _.each(rowDOMs, function(rowDOM){
        var $row = $(rowDOM);
        callback($row);
    });	
}

Section.resetRowOrder = function(secInfo){
	var index = 0;
    Section.traverseRowDOM (secInfo, function($row){
        var rowID = $row.attr('id');
        TemplateData.Row.setOrder(rowID, index); 
        index++;
    });
}

Section.addRow = function (secInfo, rowInf) {
	var rowInfo = TemplateData.Row.register(secInfo, rowInf);
	return Structure.Row.render(rowInfo);
}



export {Section};