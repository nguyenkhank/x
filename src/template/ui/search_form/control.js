import {TemplateData} from 'template/components/template_data';
import {Hooks} from 'template/components/hooks';
import {Sidebar} from 'template/ui/sidebar';
import {Render} from './render';
import {Structure} from './structure';

var Control = {};

var initEvents = function(controlInfo) {
	$('#' + controlInfo.id ).dblclick(function(){
		Sidebar.FieldEditor.enable(controlInfo);
	})
}

Control.render = function(controlInfo){
	// hook
	var b4Run = Hooks.SearchForm.Control.beforeRender.runSequence(controlInfo);
    if(b4Run === false) {
        return false;
    } 
    // end hook 

	Render.appendControl(controlInfo); 	// APPEND TO DOM
    Sidebar.disableControl('Search', controlInfo.group, controlInfo.name);
    initEvents(controlInfo);

    // hook
	Hooks.SearchForm.Control.afterRender.run(controlInfo);
	// end hook 
    return controlInfo;
}


Control.refresh = function(controlInfo){
	Render.refreshControl(controlInfo);
	initEvents(controlInfo);
}


Control.remove = function(controlInf){
	var controlInfo = TemplateData.Control.get(controlInf);

	// hook
	var b4Run = Hooks.SearchForm.Control.beforeRemove.runSequence(controlInfo);
    if(b4Run === false) {
        return false;
    } 
    // end hook 

	$('#' + controlInfo.id).remove();
	TemplateData.Control.remove(controlInfo); // remove data

	// hook
	Hooks.SearchForm.Control.afterRemove.run(controlInfo);
	// end hook 
    return controlInfo;
}



export { Control }