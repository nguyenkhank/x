var  SearchTypeControlTemplate = `<div id="<%= id %>" class="control-wrapper col-xs-<%= cols %>" title="<%= name %>">
     <div class="btn-group"> 
     	<% _.each(options, function(field){ %>
     		<button type="button" class="btn btn-sm btn-default" aria-label="Left Align">
	     		<%= field.text %>
	     	</button> 
     	<% }) %>
    </div>
</div>`;

export {SearchTypeControlTemplate}