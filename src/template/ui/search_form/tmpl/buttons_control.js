var  ButtonsControlTemplate = `<div id="<%= id %>" class="control-wrapper col-xs-<%= cols %>" title="<%= name %>">
        <button class="btn btn-default btn-sm">Search</button> <button class="btn btn-default btn-sm">Reset</button> 
        <span class="btn-advanced-search" data-control="btn-toggle"> 
            <span class="text-toggle">Show</span>
            <span class="text-toggle hide">Hide</span> Advanced Search
        </span> 
    </div>`;


export { ButtonsControlTemplate }
