var  DateControlTemplate = `<div id="<%= id %>" class="control-wrapper col-xs-<%= cols %>" title="<%= name %>">
        <div class="form-horizontal">
            <div class="row form-horizontal">
                <div class="col-xs-<%= labelCols %>"  style="text-align: <%= labelAlign %>">
                    <label class="control-label"><%= labelText %></label>
                </div>
                <div class="col-xs-<%= (12 - labelCols) %>">
                    <div class="input-group">
                        <input type="text" class="form-control input-sm" name="<%= name %>" placeholder="<%= name %>"/>
                        <% if(display === 'date') { %>
                            <span class="input-group-addon input-sm"><i class="glyphicon glyphicon-calendar"></i></span>
                        <% } else { %>
                            <span class="input-group-addon input-sm"><i class="glyphicon glyphicon-time"></i></span>
                        <% } %>
                    </div>
                </div>
            </div>
        </div>
    </div>`;

export {DateControlTemplate}
