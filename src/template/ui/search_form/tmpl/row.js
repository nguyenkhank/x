var  RowTemplate = `<div id="<%= id %>" class="row-wrapper">
						<div class="row-header">
				            <span class="row-order"><i class="fa fa-arrows-alt"></i></span>
				            <span class="row-tools pull-right">
				            	<% if (noAction !== true) { %>
			                    	<i class="fa fa-remove removeRow"></i>
			                    <% } %>
			                </span>
				       	</div> <!-- end header -->
				       	<div class="row-body" id="row-body-<%= id %>">
				       		
				       	</div> <!-- end body -->
					</div>`;

export {RowTemplate}