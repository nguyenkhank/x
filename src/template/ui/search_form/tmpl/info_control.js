var  InfoControlTemplate = `<div id="<%= id %>" class="control-wrapper col-xs-<%= cols %>" title="<%= name %>">
       <span class="fa fa-info-circle fa-fw" data-control="search-tooltip" id="documents_info" aria-hidden="true" 
            data-toggle="tooltip" 
            data-placement="bottom" title="" 
            data-original-title="To search for multiple Document IDs use the Enter key to separate one Document ID from another. For example, 1234 <Enter> 4567." 
            aria-describedby="tooltip364252"></span>
    </div>`;

export {InfoControlTemplate}