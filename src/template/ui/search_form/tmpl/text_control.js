var  TextControlTemplate = `<div id="<%= id %>" class="control-wrapper col-xs-<%= cols %>" title="<%= name %>">
        <div class="row form-horizontal">
            <div class="col-xs-<%= labelCols %>"  style="text-align: <%= labelAlign %>">
                <label class="control-label"> 
                    <% if(display == 'tags') { %>
                        <i class="fa fa-tags"></i>
                    <% } else {  %>
                        <i class="fa fa-pencil"></i>
                    <% } %>
                    <%= labelText %>
                </label>
            </div>
            <div class="col-xs-<%= (12 - labelCols) %>">
                <input type="text" class="form-control input-sm" name="<%= name %>" placeholder="<%= name %>"/>
            </div>
        </div>
    </div>`;


module.exports = {
    TextControlTemplate: TextControlTemplate,
}    

