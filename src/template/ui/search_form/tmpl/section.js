var  SectionTemplate = `<div id="<%= id %>" class="section-wrapper panel panel-default">
	<div class="section-header panel-heading">
		<span class="section-title">
			<input value="<%= title %>" disabled class="input-section-title" />
		</span>
		
		<span  class="section-tools pull-right">
            <span class="addRow"><i class="fa fa-plus"></i> Row</span>
            <span class="configSection"><i class="fa fa-cog"></i></span>   
            <span data-toggle="collapse" id="showhide_<%= id %>" data-target="#sec-body-<%= id %>">
            	<i class="field-arrow fa fa-chevron-down"></i>
            </span>
        </span>
	</div>
	<div id="sec-body-<%= id %>" class="panel-body section-body ui-sortable collapse in" style="" aria-expanded="true"></div>
</div>`;

// id 
// title

module.exports = {
	SectionTemplate: SectionTemplate,
}