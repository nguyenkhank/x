var  SelectControlTemplate = `<div id="<%= id %>" class="control-wrapper col-xs-<%= cols %>" title="<%= name %>">
    <div class="form-horizontal">
        <div class="row form-horizontal">
            <div class="col-xs-<%= labelCols %>"  style="text-align: <%= labelAlign %>">
                <label class="control-label">
                    <% if(display == 'enum') { %>
                        <i class="fa fa-list"></i>
                    <% } else if (display == 'select') {  %>
                        <i class="fa fa-database"></i>
                    <% } else { %>
                        <i class="fa fa-exchange"></i>
                    <% } %>
                    <%= labelText %>
                </label>
            </div>
            <div class="col-xs-<%= (12 - labelCols) %>">
                <select class="form-control input-sm" name="<%= name %>" placeholder="<%= name %>" ></select>
            </div>
        </div>
    </div>
</div>`;

export {SelectControlTemplate}

