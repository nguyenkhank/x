import {TemplateData} from 'template/components/template_data';
import {Hooks} from 'template/components/hooks';
import {ConfigData} from 'template/components/config_data';

import {Structure} from './structure';
import {Control} from './control';
import {Render} from './render';


var Row = {};

var initEvents = function(rowInfo) {
    if(rowInfo.noAction === true) {
        return;
    }

	// sort control
	var connectSelector = '#' + rowInfo.sectionID +  " .row-body";
	$('#' + rowInfo.id + ' .row-body').sortable({
        delay: 100,
        helper: 'clone',
        cursor: "move",
        connectWith: connectSelector,
        update: function( event, ui ){
            var $newRowContain = ui.item.parents('.row-wrapper');
            var newRowID = $newRowContain.attr('id');

            var controlID = ui.item.attr('id');

            if(_.isUndefined(newRowID)) { //remove get nothing here
                return;
            }

            Structure.Row.resetControlOrder(newRowID);
            if(ui.sender == null){
                return;
            }

            var oldRowID = ui.sender.parents('.row-wrapper').attr('id');
            Structure.Row.resetControlOrder(oldRowID);

            TemplateData.Control.setRowID(controlID, newRowID);

        }
    }).disableSelection();
     // end sort

    function validateControl(rowInf, $ele){
        return true;
    }
    $('#' + rowInfo.id + ' .row-body').droppable({
        accept: ".fieldSelector",
        over: function( event, ui ) {
            var res = validateControl(rowInfo,  ui.draggable);
            if(!res) {
                ui.helper.css('border', '1px solid red');
            } else {
                ui.helper.css('border', '1px solid green');
            }
        },
        drop: function (event, ui) {
            var $ele = ui.draggable;
            var res = validateControl(rowInfo,  $ele);
            if(!res) {
                return; 
            }
            var fieldname = $ele.data('fieldname');
            var group = $ele.parent('.groupSelector').data('fieldgroup');

            var fieldConfig = ConfigData.getFieldConfig(group, fieldname);
            if(_.isEmpty(fieldConfig)) {
                console.warn('Cant find field config!', group, fieldname);
                return;
            }

            var controlInfo = Row.addControl(rowInfo, group, fieldConfig);   // APPEND TO DOM
        }
    });
}

Row.render = function(rowInfo){
	// hook
	var b4Run = Hooks.SearchForm.Row.beforeRender.runSequence(rowInfo); 
    if(b4Run === false) {
        return false;
    } 
    // end hook 

	Render.appendRow(rowInfo); 	// APPEND TO DOM
    initEvents(rowInfo);

     // hook
    Hooks.SearchForm.Row.afterRender.run(rowInfo);
	// end hook 
    return rowInfo;
}

/// ROW
Row.remove = function(rowInf){
	var rowInfo = TemplateData.Row.get(rowInf);
	// hook
	var b4Run = Hooks.SearchForm.Row.beforeRemove.runSequence(rowInfo);
    if(b4Run === false) {
        return false;
    } 
    // end hook 

	$('#' + rowInfo.id).remove()  // remove dom
	TemplateData.Row.remove(rowInfo); // remove data

	// hook
    Hooks.SearchForm.Row.afterRemove.run(rowInfo);
	// end hook 
	return rowInfo;
}

Row.forceRemove = function(rowInf) {
    var controlsInfo = TemplateData.Row.getControls(rowInf);
    _.each(controlsInfo, function(controlInfo){
        Control.remove(controlInfo);
    });
    Row.remove(rowInf);
}


Row.addControl = function (rowInfo, group, fieldConfig) {
    var basicInfo = {
        name: fieldConfig.FieldName,
        labelText: fieldConfig.Title,
        type: fieldConfig.Type,
        group: group,
    };

    // hook
    var b4Run = Hooks.SearchForm.Control.beforeAdd.runSequence(basicInfo, fieldConfig, rowInfo);
    if(b4Run === false) {
        return false;
    } 
    // end hook 

	var controlInfo = TemplateData.Control.register(rowInfo, basicInfo);
	Structure.Control.render(controlInfo);
    
    // hook
    Hooks.SearchForm.Control.afterAdd.run(controlInfo, rowInfo);
    // end hook 
    return controlInfo;
}

Row.traverseControlDOM = function(rowInf, callback){
	var rowInfo = TemplateData.Row.get(rowInf);
	var controlDOMs =  $('#row-body-' + rowInfo.id).find('.control-wrapper');
    _.each(controlDOMs, function(controlDOM){
        var $control = $(controlDOM);
        callback($control);
    });	
}

Row.resetControlOrder = function(rowInf){
	var index = 0;
    Row.traverseControlDOM (rowInf, function($control){
        var controlID = $control.attr('id');
        TemplateData.Control.setOrder(controlID, index); 
        index++;
    });
}


export {Row};


