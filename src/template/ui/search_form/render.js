import {RenderFactory} from 'base/render_factory';
import {SectionTemplate} from './tmpl/section';
import {RowTemplate} from './tmpl/row';

import {TextControlTemplate} from './tmpl/text_control';
import {DateControlTemplate} from './tmpl/date_control';
import {SelectControlTemplate} from './tmpl/select_control';
import {ButtonsControlTemplate} from './tmpl/buttons_control';
import {InfoControlTemplate} from './tmpl/info_control';

import {SearchTypeControlTemplate} from './tmpl/searchtype_control';

var Render = RenderFactory.create();

var getControlTemplate = function(controlInfo){
	// klog(controlInfo)
	switch(controlInfo.display) {
		case 'datetimerange':
		case 'searchby':
		case 'select':
		case 'enum':
			return SelectControlTemplate;

		case 'searchtype':
			return SearchTypeControlTemplate;

		case 'time':
		case 'date':
			return DateControlTemplate;

		case 'buttons':
			return ButtonsControlTemplate;

		case 'info':
			return InfoControlTemplate;
			
		case 'text':
		case 'number':
		default:
			return TextControlTemplate;
	}
}

Render.appendControl = function(controlInfo){
	var template = getControlTemplate(controlInfo);
	var html = Render.buildTemplate(template, controlInfo);
	$('#row-body-' + controlInfo.rowID).append(html);
}

Render.refreshControl = function(controlInfo){
	var template = getControlTemplate(controlInfo);
	var html = Render.buildTemplate(template, controlInfo);
	$('#' + controlInfo.id).replaceWith(html);
}

Render.appendSection = function(sectionInfo){
	var html = Render.buildTemplate(SectionTemplate, sectionInfo);
	$('#searchFormWrapper').append(html);
}

Render.appendRow = function(rowInfo){
	var html = Render.buildTemplate(RowTemplate, rowInfo);
	$('#sec-body-' + rowInfo.sectionID).append(html);
}

export {
	Render
}