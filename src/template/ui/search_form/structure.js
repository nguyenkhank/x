import {Section} from './section';
import {Row} from './row';
import {Control} from './control';

var Structure = {};

Structure.Section = Section;
Structure.Row = Row;
Structure.Control = Control;

export { Structure }