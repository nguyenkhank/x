import {ConfigData} from 'template/components/config_data';
import {TemplateData} from 'template/components/template_data';


import {Sidebar} from '../sidebar';
import {Structure} from './structure';


var SearchForm = {};

SearchForm.Structure = Structure;

/*
*   INIT EVENT FOR SEARCH
*/
SearchForm.init = function(){
    $('#showSearchSelector').click(function(){
        Sidebar.showPanel('searchSelectorPanel');
    });
}

// render from data of template
SearchForm.render = function(){
	// get data 
	var confs = [{bundle: 'Section'}, {bundle: 'Row'}, {bundle: 'Control'}, ];
	_.each(confs, function(conf){
		var items = TemplateData[conf.bundle].getData();
		var sortedItems = _.sortBy(items, function(item){
			return parseInt(item.order);
		});
		_.each(sortedItems, function(itemInfo){
			SearchForm.Structure[conf.bundle].render(itemInfo);
		})
	})
}


export { SearchForm };