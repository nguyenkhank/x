import {Hooks} from 'template/components/hooks';
import {TemplateData} from 'template/components/template_data';
import {Sidebar} from 'template/ui/sidebar';
import {Render} from './render';


var Structure = {};

var initEvents = function(columnInfo){
	$('#' + columnInfo.id ).dblclick(function(e){
		Sidebar.ColumnEditor.enable(columnInfo);
	});
}


Structure.addColumn = function(columnInfo) {
	// hook
	var b4Run = Hooks.ResultTable.beforeAddColumn.runSequence(columnInfo);
    if(b4Run === false) {
        return false;
    } 
    // end hook 
	Render.appendColumn(columnInfo);
    Sidebar.disableControl('Result', columnInfo.group, columnInfo.name);

	initEvents(columnInfo);

	// hook
	Hooks.ResultTable.afterAddColumn.run(columnInfo);
	// end hook 
	return columnInfo;
};

Structure.removeColumn = function(columnInf){
	var columnInfo = TemplateData.Column.get(columnInf);
	// hook
	var b4Run = Hooks.ResultTable.beforeRemoveColumn.runSequence(columnInfo);
    if(b4Run === false) {
        return false;
    } 
    // end hook 
	
	$('#' + columnInfo.id).remove();
	TemplateData.Column.remove(columnInfo); // remove data

	// hook
	Hooks.ResultTable.afterRemoveColumn.run(columnInfo);
	// end hook 
    return columnInfo;
};


Structure.refreshColumn = function(columnInfo){
	Render.refreshColumn(columnInfo);
	initEvents(columnInfo);
	return columnInfo;
};



Structure.traverseColumnDOM = function(callback){
	var DOMs =  $('#resultColumnWrapper').find('.column-item');
    _.each(DOMs, function(DOM){
        var $item = $(DOM);
        callback($item);
    });	
}

Structure.resetColumnOrder = function(){
	var index = 0;
    Structure.traverseColumnDOM (function($column){
        var columnID = $column.attr('id');
        TemplateData.Column.setOrder(columnID, index); 
        index++;
    });
}


export { Structure }