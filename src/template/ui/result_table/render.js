import {Render as BaseRender} from  '../render';

import {TableColumn} from  './tmpl/table_column';

var Render = {};

Render.appendColumn = function(columnInfo){
	var html = BaseRender.buildTemplate(TableColumn, columnInfo);
	$('#resultColumnWrapper').append(html);
}

Render.refreshColumn = function(columnInfo){
	var html = BaseRender.buildTemplate(TableColumn, columnInfo);
	$('#' + columnInfo.id).replaceWith(html);
}


export {
	Render
}