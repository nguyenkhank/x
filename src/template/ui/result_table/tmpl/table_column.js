var Template = `<div class="column-item" id="<%= id %>">
					<span style="<%= StyleBuilder.BIU(headerBold, headerItalic, headerUnderline) %>"><%= headerText %></span>
					<% if (hide) { %> 
						<i class="fa fa-eye-slash"></i> 
					<% } else { %>
						<% if (sort) { %> 
							<i class="fa fa-sort"></i> 
						<% } %>
						<% if (filter) { %> 
							<i class="fa fa-filter"></i> 
						<% } %>
					<% } %>
			</div>`;


module.exports = {
	TableColumn: Template,
}