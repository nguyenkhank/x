import {ConfigData} from 'template/components/config_data';
import {TemplateData} from 'template/components/template_data';
import {Sidebar} from '../sidebar';
import {Structure} from './structure';

var ResultTable = {};

ResultTable.init = function() {
	$('#resultColumnWrapper').sortable({
        delay: 100,
        helper: 'clone',
        cursor: "move",
        // connectWith: connectSelector,
        update: function( event, ui ){
        	klog('SORTED')
        	Structure.resetColumnOrder();
        }
    }).disableSelection();

    $('#resultColumnWrapper').droppable({
        accept: ".columnSelector",
        over: function( event, ui ) {
            ui.helper.css('border', '1px solid green');
        },
        drop: function (event, ui) {
            var $ele = ui.draggable;
            var fieldname = $ele.data('fieldname');
            var group = $ele.parent('.groupSelector').data('fieldgroup');
            var fieldConfig = ConfigData.getFieldConfig(group, fieldname);
            if(_.isEmpty(fieldConfig)) {
                console.warn('Cant find field config!', group, fieldname);
                return;
            }
            var basicInfo = {
                name: fieldConfig.FieldName,
                group: group,
                type: fieldConfig.GUIType,
                // 
                headerText: fieldConfig.Title,
            };
            var columnInfo = TemplateData.Column.register(basicInfo);   // APPEND TO DOM
            Structure.addColumn(columnInfo);
        }
    });
}


ResultTable.render = function(){
    var items = TemplateData.Column.getData();
    var sortedItems = _.sortBy(items, function(item){
        return parseInt(item.order);
    });
    _.each(sortedItems, function(itemInfo){
        ResultTable.Structure.addColumn(itemInfo);
    })
}

ResultTable.Structure = Structure;
export { ResultTable };