import {RenderFactory} from 'base/render_factory';
import {BaseTemplate} from './tmpl/base';
import {Sidebar} from './sidebar';
import {SearchForm} from './search_form';
import {ResultTable} from './result_table';
import {TemplateData} from 'template/components/template_data';

var Render = RenderFactory.create();

Render.drawStructure = function(containerID){
	var html = Render.buildTemplate(BaseTemplate, {});
	$('#' + containerID).html(html);
}


Render.draw = function(data){
	// set data 
	TemplateData.setData(data);
	// end set data

	SearchForm.render();
	ResultTable.render();
}


Render.init = function(containerID){
	Render.drawStructure(containerID);
	Sidebar.init();
	SearchForm.init();
	ResultTable.init();
}

export { Render };