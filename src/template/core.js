import {Hooks} from './components/hooks';
import {ConfigData} from './components/config_data';
import {Render} from 'template/ui/render';
import {FieldEditorHandler} from './handler/field_editor_handler';
import {ColumnEditorHandler} from './handler/column_editor_handler';
import {SearchFormHandler} from './handler/search_form_handler';
import {LogHandler} from './handler/log_handler';


var Editor = {
	highlightItem: function(itemInfo){
		// klog('CONTROL / COLUMN INFO', itemInfo);
		Editor.unhighlightItem();
		$('#' + itemInfo.id).addClass('activeItem').css('border', '1px solid #32c5d2');
	},
	unhighlightItem: function(){
		$('.activeItem').css('border', '1px solid #ddd').removeClass('activeItem');
	},
};



var Core = {};
Core.init = function (option) {
	if(!_.has(option, 'containerID')) {
		console.error('Option required `containerID`!');
		return;
	}

	if(_.has(option, 'hooks')) {
		Hooks.register(option.hooks);
		delete option.hooks;
	}

	if(_.has(option, 'dbMapping')) {
		ConfigData.setDBMapping(option.dbMapping);
		delete option.dbMapping;
	}

	ConfigData.setUserOption(option);

	
	///////// register
	Render.init(option.containerID);

	// editor
	Hooks.push('ColumnEditor.afterActive', Editor.highlightItem);
	Hooks.push('ColumnEditor.afterClose', Editor.unhighlightItem);

	Hooks.push('FieldEditor.afterActive',  Editor.highlightItem);
	Hooks.push('FieldEditor.afterClose', Editor.unhighlightItem);


	Hooks.push('ResultTable.beforeAddColumn', function(columnInfo){
		// more info about group
		if(ConfigData.hasGroupRequired(columnInfo.group)) {
			columnInfo.through_group = ConfigData.getGroupRequired(columnInfo.group);
		}
	});
	

	FieldEditorHandler.init(Hooks);
	ColumnEditorHandler.init(Hooks);
	SearchFormHandler.init(Hooks);


	if(ConfigData.isModeDev()) {
		LogHandler.init(Hooks);
	}
}

export {Core};