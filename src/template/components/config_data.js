import {CustomControls} from './constants';
import {ConfigDataFactory} from 'base/config_data_factory';

var option = {
	containerID: null,
	mode: 'DEV',

	// dbMapping option
	Groups: null,
	Fields: null,
	Dropdown: null,
};

var ConfigData = ConfigDataFactory.create(option);

ConfigData.Query = {};


var SidebarGroup = {};
var ResultGroup = {};

var CustomGroup = {
	GroupName: '_custom',
	Title: 'Special Controls',
};

ConfigData.isModeDev = function(){
	return option.mode === 'DEV';
}

// group 
ConfigData.groupDropdown = function(){
	return _.map(option.Groups, function(groupInfo){
		return {
			id: groupInfo.GroupName,
			text: groupInfo.Title,
		}
	});
}
// field
ConfigData.fieldDropdown = function(groupName, type){
	var criteria = {GroupName: groupName};
	var fields = _.where(option.Fields, criteria);

	return _.map(fields, function(fieldInfo){
		return {
			id: fieldInfo.FieldName,
			text: fieldInfo.FieldName,
		}
	});
}


ConfigData.getFieldConfig = function(groupName, fieldName){
	var criteria = {GroupName: groupName, FieldName: fieldName};
	return _.findWhere(option.Fields, criteria);
};



ConfigData.getGroupRequired = function(groupName) {
	var criteria = {GroupName: groupName};
	var groupInfo = _.findWhere(option.Groups, criteria);
	return _.accessStr(groupInfo, 'Meta.GroupRequired');
}


ConfigData.hasGroupRequired = function(groupName) {
	var GroupRequired = ConfigData.getGroupRequired(groupName);
	return !!GroupRequired;
}


ConfigData.getDropdown = function(groupName, fieldName){
	var criteria = {GroupName: groupName, FieldName: fieldName};
	return _.findWhere(option.Dropdown, criteria);
}

ConfigData.hasDropdown = function(groupName, fieldName){
	var obj =  ConfigData.getDropdown(groupName, fieldName);
	return !_.isEmpty(obj);
}

ConfigData.getDateFields = function(){
	return _.filter(option.Fields, function(fieldConfig){
		return _.contains(['date', 'datetime'], fieldConfig.Type);
	});
}

ConfigData.dataSidebarSearch = function(){
	var res = [];
	_.each(option.Groups, function(group){
		if(!_.isEmpty(SidebarGroup[group.GroupName])) {
			var obj = _.extend(group);
			obj.Panel = 'Search';
			obj.Fields = SidebarGroup[group.GroupName];
			res.push(obj);
		}
	});
	return res;
}

ConfigData.dataSidebarResult = function(){
	var res = [];
	_.each(option.Groups, function(group){
		if(!_.isEmpty(ResultGroup[group.GroupName])) {
			var obj = _.extend(group);
			obj.Panel = 'Result';
			obj.Fields = ResultGroup[group.GroupName];
			res.push(obj);
		}
	})
	return res;
}

ConfigData.setDBMapping = function (dbMapping) {
	dbMapping.Groups.push(CustomGroup);

	dbMapping.Fields = dbMapping.Fields.concat(CustomControls);
	// PUSH MORE DATA
	var handleGroupField = function(groupData){
		SidebarGroup[groupData.GroupName] = [];
		ResultGroup[groupData.GroupName] = [];

		var fields = groupFields[groupData.GroupName];
		_.each(fields, function(field){
			if(!field.Role) {
				return;
			}

			var roles = field.Role.split(',');
			if(_.contains(roles, 'search_form')) {
				if(!_.contains(['date', 'datetime'], field.Type)) { // ignore 
					SidebarGroup[groupData.GroupName].push(field);
				}
			}

			// table result
			if(_.contains(roles, 'table_result')) {
				ResultGroup[groupData.GroupName].push(field);
			}
		});
	}


	var groupFields = _.groupBy(dbMapping.Fields, 'GroupName');
	_.each(dbMapping.Groups, function(groupData){
		groupData.hasFields = _.has(groupFields, groupData.GroupName);
		if(!groupData.hasFields ){
			console.warn('Group: ' + groupData.GroupName + ' has no fields config!');
			return;
		}
		handleGroupField(groupData);
	});

	_.each(dbMapping.Groups, function(groupInfo){
		if(groupInfo.Meta) {
			groupInfo.Meta = JSON.parse(groupInfo.Meta);
		}
	})

	option.Groups = dbMapping.Groups;
	option.Fields = dbMapping.Fields;
	option.Dropdown = dbMapping.Dropdown;
}

ConfigData.SidebarGroup = SidebarGroup;
ConfigData.ResultGroup = ResultGroup;

export { 
	ConfigData 
};