var FIXED_TYPES = ['enum', 'date', 'time', 'info', 'buttons', 'searchtype', 'searchby', 'datetimerange' ]; // type, display is the same

var LABEL_TYPES = {
    enum: 'Enum', 
    date: 'Date',
    time: 'Time',
    info: 'Information', 
    buttons: 'Buttons',
    searchby: 'Search By', 
    searchtype : 'Search Type', 
    datetimerange: 'DateTime Range',
};


var CONTROL_ATTR_DEFAULT = {
    info: {
        cols: '1',
        tooltip: 'To search for multiple Document IDs use the Enter key to separate one Document ID from another. For example, 1234 <Enter> 4567.',
    }, 
    datetimerange: {
        labelText: 'Date Type',
        rows: [],
        fields: [],
    }, 
    searchby: {
        cols: '5',
        fields: [],
    },
    date: { 
        cols: '3',
    },
    time:{
        cols: '3'
    },
    searchtype: {
        cols: '9',
        options: [{text: '1st Action'}],
    }
}

var CustomControls = [
    {
        FieldName: 'Info',
        GroupName: '_custom',
        Title: 'Information',
        Type: 'info',
        Role: 'search_form',
    },
    {
        FieldName: 'Buttons',
        GroupName: '_custom',
        Title: 'Buttons',
        Type: 'buttons',
        Role: 'search_form',
    }, {
    
        FieldName: 'SearchBy',
        GroupName: '_custom',
        Title: 'Search By',
        Type: 'searchby',
        Role: 'search_form',
    }, {
        FieldName: 'DateTimeRange',
        GroupName: '_custom',
        Title: 'DateTime Range',
        Type: 'datetimerange',
        Role: 'search_form',
    },  {
        FieldName: 'SearchType',
        GroupName: '_custom',
        Title: 'Search Type',
        Type: 'searchtype',
        Role: 'search_form',
    }
];

window.PageHTMLBuilder = function () {
    var instance = {};

    instance.fieldIcon = function(type){
        switch (type) {
            case 'searchtype':
                return 'play';
            case 'searchby':
                return 'exchange';

            case 'buttons':
            case 'searchtype':
                return 'send';

            case 'info':
                return 'info-circle';

            case 'datetimerange':
            case 'date':
            case 'datetime':
                return 'calendar-o';
            
            case 'select':
                return 'list-ul';
            case 'select2':
            case 'multiselect':
                return 'database';
            case 'checkbox':
                return 'check-square-o';
            case 'number':
                return 'calculator';
            case 'file':
                return 'upload';
            case 'lookup':
                return 'search';
            case 'text':
            default:
                return 'pencil-square-o';
        }
    }

    return instance;
}();

var SECTION_DATA = {
    id: null, // gen id 
    title: null,
    name: null,
    order: 0, // gen 
    indexRow: 0, // gen
};

var ROW_DATA = {
    id: null,
    sectionID: null,
    order: null,
    indexControl: 0,
    noAction: false,
};

var CONTROL_DATA = {
    // no config
    id: null,
    name: null,
    type: null,
    group: null,
    // end no config

    cols: '5',
    align: 'left',
    display: '',
    multiple: false,

    // POSITION
    rowID: null, 
    order: null,
    // label
    labelText: null,
    labelCols: '4',
    labelAlign: 'right',
    // labelPosition: 'left',
    // labelItalic: false,
    // labelUnderline: false,
    // labelBold: false,
    // validate

    defaultValue: '',

    dataList: [], // for enum

    required: false,
    readonly: false,


    sourceGroup: null,
    columnSelected: null,
    displayFormat: null,
    searchAjax: false,
};

var COLUMN_DATA = {
    id: null, 
    name: null,
    type: null,
    group: null,
    order: null,

    format: null,
    hide: false,
    sort: true,
    // filter 
    filter: true,
    filterType: 'text',
    selectValue: null,
    // end filter
    width: 'auto',
    headerBold: true,
    headerItalic: false,
    headerUnderline: false,
    headerText: '',
    // data formmat
    dataFormat: 'raw',
    align: 'left',

    // validate
    required: false,
};

export {
    CustomControls,
    CONTROL_ATTR_DEFAULT, 
    LABEL_TYPES,
    FIXED_TYPES,
    // default data
    SECTION_DATA,
    ROW_DATA,
    CONTROL_DATA,
    COLUMN_DATA,
}