import {SECTION_DATA, ROW_DATA, CONTROL_DATA, COLUMN_DATA} from './constants';
import {DataList} from 'base/data_list'

const SECTION_PREFIX = 'sec_';
const CONTROL_PREFIX = 'ctrl_';
const COLUMN_PREFIX = 'col_';

// public export
var TemplateData = {};
// section


class SectionList extends DataList {
	getByName(name){
		return _.findWhere(this.bundle, {name: name});
	}

	registerNewID(){
		if(this.bundle.length == 0){
            return 1;
        }
        var maxId = 0;
        _.each(this.bundle, function(section) {
            var idNum = parseInt(section.id.replace( SECTION_PREFIX , '')); // remove first string
            if(maxId < idNum) {
                maxId = idNum;
            }
        });
        return ++maxId;
	}

	register (basicInfo) {
		var tmplSection =  _.extend({}, SECTION_DATA);  
		tmplSection = _.extend(tmplSection, basicInfo);  

		tmplSection.id = SECTION_PREFIX + this.registerNewID();
		tmplSection.order = this.bundle.length; // start from 0

		this.bundle.push(tmplSection);
		return tmplSection;	
	}

	getRows(sectionID){
		var condition = {sectionID: sectionID};
        var rows = _.where(RowTemplate.getData(), condition);
        return _.sortBy(this.bundle, 'order');
	}

	countRows(sectionID){
		var rows = SectionTemplate.getRows(sectionID);
		return rows.length;
	}

	get(info) {
		if(_.isObject(info)) {
			if(_.has(info, 'name')) {
				return this.getByName(info.name);
			}
		}
		return super.get(info);
  	}
}

class RowList extends DataList {
	register(secInfo, basicInfo){
		var tmplRow =  _.clone(ROW_DATA);
		if(_.isObject(basicInfo)) {
			_.extend(tmplRow, basicInfo);
		}

		var sectionInfo = SectionTemplate.get(secInfo);
		if(sectionInfo == null) {
            console.error('Not found Section Info', secInfo);
            return;
        }
        var sectionID = sectionInfo.id;
        ++sectionInfo.indexRow; // increase         
        // 
        tmplRow.id = sectionID + '_' + sectionInfo.indexRow;
        tmplRow.sectionID = sectionID;
        tmplRow.order = sectionInfo.indexRow; // SectionTemplate.countRows(sectionInfo.id); // end
       	//
       	this.bundle.push(tmplRow);
        return tmplRow;
	}

	getControls(info){
		var rowInfo = this.get(info);
		var objects =  _.where(ControlTemplate.getData(), {rowID: rowInfo.id});
        return _.sortBy(objects, 'order');
	}
	
	countControls(info){
		var controls = this.getControls(info);
		return controls.length;
	}

	getSectionInfo(info){
		var rowInfo = this.get(info);
		if(rowInfo == null) {
			return;
		}
		return SectionTemplate.get(rowInfo.sectionID);
	}
}

class ControlList extends DataList {

	getByNameAndGroup(name, group){
		var criteria = {name: name, group: group};
		return _.findWhere(this.bundle, criteria);
	}


	getByType(type) {
		var criteria = {type: type};
		return _.findWhere(this.bundle, criteria);
	}

	get(info) {
		if(_.isObject(info)) {
			if(!_.has(info, 'id')) {
				// return this.getByName(info.name);
				if(_.has(info, 'group') && _.has(info, 'name')) {
					return this.getByNameAndGroup(info.name, info.group);
				}
			}
		}
		return super.get(info);
  	}

	getSection(info) {
		var rowInfo = this.getRow(info);
		return RowTemplate.getSectionInfo(rowInfo);
	}

	getRow(info) {
		var controlInfo = this.get(info);
		return RowTemplate.get(controlInfo.rowID);
	}

	setRowID(info, rowID) {
		this.setAttribute(info, 'rowID', rowID);
	}

	register(rowInf, basicInfo){
		var rowInfo = RowTemplate.get(rowInf);
		if(rowInfo == null) {
            console.error('Not found Row Info', rowInf);
            return;
        }
		// basic info include (name + type)
        if(!_.has(basicInfo, 'name') || !_.has(basicInfo, 'type')) {
        	console.error('Lack of control info', rowInf);
            return;
        }

        ++rowInfo.indexControl; // increase   
		var tmplControl = _.clone(CONTROL_DATA);
		_.extend(tmplControl, basicInfo); 
		// 
		tmplControl.rowID = rowInf.id;
		tmplControl.id = CONTROL_PREFIX + tmplControl.group + '_' + tmplControl.name;
		tmplControl.order = rowInfo.indexControl; // RowTemplate.countControls(rowInfo.id);
		// 

		this.bundle.push(tmplControl);
        return tmplControl;
	}
}


var SectionTemplate = new SectionList([]);
var RowTemplate = new RowList([]);
var ControlTemplate = new ControlList([]);
// end section 

class ColumnList extends DataList {
	getFilters(){
		var columns = _.where(this.bundle, {filter: true});
		return _.sortBy(columns, 'order');
	}

	register(basicInfo){
		var tmplColumn = _.clone(COLUMN_DATA);
		_.extend(tmplColumn, basicInfo); 
		//
		tmplColumn.id = COLUMN_PREFIX + tmplColumn.group + '_' + tmplColumn.name;
		tmplColumn.order = this.bundle.length;
		// 
		this.bundle.push(tmplColumn);
        return tmplColumn;
	}


	getByNameAndGroup(name, group){
		var criteria = {name: name, group: group};
		return _.findWhere(this.bundle, criteria);
	}

	get(info) {
		if(_.isObject(info)) {
			if(!_.has(info, 'id')) {
				// return this.getByName(info.name);
				if(_.has(info, 'group') && _.has(info, 'name')) {
					return this.getByNameAndGroup();
				}
			}
		}
		return super.get(info);
  	}
}

var ColumnTemplate = new ColumnList([]);
// assign into main object
TemplateData.Section = SectionTemplate;
TemplateData.Row = RowTemplate;
TemplateData.Control = ControlTemplate;
// column
TemplateData.Column = ColumnTemplate;

TemplateData.setData = function(data) {
	if(_.has(data, 'sections')) {
		// correct 
		_.each(data.sections, function(entry){
			_.defaults(entry, CONTROL_DATA);
		});
		// end correct
		TemplateData.Section.setData(data.sections);
	}

	if(_.has(data, 'rows')) {
		// correct 
		_.each(data.rows, function(entry){
			_.defaults(entry, ROW_DATA);
		});
		// end correct
		TemplateData.Row.setData(data.rows);
	}

	if(_.has(data, 'controls')) {
		// correct 
		_.each(data.controls, function(entry){
			_.defaults(entry, CONTROL_DATA);
		});
		// end correct
		TemplateData.Control.setData(data.controls);
	}

	if(_.has(data, 'columns')) {
		// correct 
		_.each(data.columns, function(entry){
			_.defaults(entry, COLUMN_DATA);
		});
		// end correct
		TemplateData.Column.setData(data.columns);
	}
}

window.SearchTemplateData = TemplateData;
export {
	TemplateData
}
