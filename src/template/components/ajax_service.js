var Service = {}

var CacheEnumBundle = new B2BECache({cacheType: 'memory'});
Service.getEnumValues = function (groupName, fieldName) {
	var keyCache = groupName + '_' + fieldName;

	var paramRequest = {group: groupName, field: fieldName};
	return AjaxService.postCache(CacheEnumBundle, keyCache, 'eform/searchservice/enum_values', paramRequest);
}



export {Service}