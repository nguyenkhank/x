import {HookListFactory} from 'base/hook_list_factory';

var Hooks = {};

// Hooks.Core = {
// 	afterInit: [],
// };



Hooks.SearchForm = {
	Section: {
		afterRender: HookListFactory.create(), 
		afterRegister: HookListFactory.create(),
	},
	Row: {
		beforeAdd: HookListFactory.create(), 
		afterAdd: HookListFactory.create(), 

		beforeRender: HookListFactory.create(), 
		afterRender: HookListFactory.create(), 

		beforeRemove: HookListFactory.create(), 
    	afterRemove: HookListFactory.create(),
	},
	Control: {
		beforeAdd: HookListFactory.create(), 
		afterAdd: HookListFactory.create(), 

		beforeRender: HookListFactory.create(), 
		afterRender: HookListFactory.create(), 

		beforeRemove: HookListFactory.create(), 
    	afterRemove: HookListFactory.create(), 
	}
};

Hooks.ResultTable = {
   	beforeRemoveColumn: HookListFactory.create(), 
    afterRemoveColumn: HookListFactory.create(), 
    beforeAddColumn: HookListFactory.create(), 
    afterAddColumn: HookListFactory.create(), 
};

Hooks.ColumnEditor = {
	beforeActive: HookListFactory.create(), 
	afterActive: HookListFactory.create(), 
	beforeApply: HookListFactory.create(), 
    afterApply: HookListFactory.create(), 
    beforeClose: HookListFactory.create(), 
    afterClose: HookListFactory.create(), 
};

Hooks.FieldEditor = {
	beforeActive: HookListFactory.create(), 
	afterActive: HookListFactory.create(), 
	beforeApply: HookListFactory.create(), 
    afterApply: HookListFactory.create(), 
    beforeClose: HookListFactory.create(), 
    afterClose: HookListFactory.create(), 
};


// Hooks.reset = function () {
// 	klog('NOT AVAILABEL')
// }

Hooks.push = function (path, func) {
	var listHook = _.accessStr(Hooks, path);
	if(listHook == null) {
		console.warn('Not found path: ' + path + ' in Hooks!');
		return false;
	}
	listHook.push(func);
}

Hooks.register = function(customHooks) {
	if(!_.isObject(customHooks)) {
        return false;
    }
    _.each(customHooks, function(customHook, path){
    	Hooks.push(path, customHook);
    });
}


klog(Hooks)

export {
	Hooks
}