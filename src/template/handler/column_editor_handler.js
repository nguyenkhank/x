import {Service as AjaxService} from 'template/components/ajax_service';
import {ConfigData} from 'template/components/config_data';

var ColumnEditorHandler = {};

ColumnEditorHandler.init = function(Hooks){
	Hooks.push('ColumnEditor.afterActive', ColumnEditorHandler.setDataFormat);
}

ColumnEditorHandler.setDataFormat = function(columnInfo, $formEditor){
	var fieldConfig = ConfigData.getFieldConfig(columnInfo.group, columnInfo.name);

	var optionDisplay = [
	     {id: 'raw', text: text('Raw')}
	];
	switch(fieldConfig.Type) {
		case 'datetime':
			optionDisplay.push({id: 'datetime', text: text('Date Time') });
		case 'date':
			optionDisplay.push({id: 'date', text: text('Date')});
			break;
		default:
			optionDisplay.push({id: 'currency', text: text('Currency')});
	}
	// set data source
	$formEditor.setDataSource('dataFormat', optionDisplay);

	// set value
	columnInfo.dataFormat = columnInfo.dataFormat || 'raw';
	$formEditor.setValues({dataFormat: columnInfo.dataFormat});
}

export {ColumnEditorHandler};