import {SearchForm} from 'template/ui/search_form';
import {Sidebar} from 'template/ui/sidebar';

import {TemplateData} from 'template/components/template_data';
import {ConfigData} from 'template/components/config_data';

import {GroupListConf} from 'template/handler/common/group_field_conf';

var CONTROL_TYPE = 'searchby';
var SearchByHandler = {};

SearchByHandler.init = function (Hooks) {
	Hooks.push('SearchForm.Control.afterAdd', afterAddControl);
	Hooks.push('SearchForm.Control.beforeRemove', beforeRemoveControl);
	Hooks.push('SearchForm.Control.afterRemove', afterRemoveControl);

	Hooks.push('FieldEditor.afterActive', showEditorUI);
	Hooks.push('FieldEditor.beforeApply', beforeApplyEditor);
}

var beforeRemoveControl = function(controlInfo){
	// will remove after parent control
	if(!(controlInfo.group == '_custom' && controlInfo.name == 'DocumentID')) { 
		return;
	}
	var parentControl = TemplateData.Control.getByType(CONTROL_TYPE);
	if(_.isEmpty(parentControl)) { // parent still exists
		return true;
	}
	// prevent 
	return false;
}

var afterRemoveControl = function(controlInfo){
	if(controlInfo.type !== CONTROL_TYPE) {
		return;
	}
	_.each(controlInfo.fields, function(fieldID){
		SearchForm.Structure.Control.remove(fieldID);
	});
}

var afterAddControl = function(controlInfo){
	if(controlInfo.type !== CONTROL_TYPE) {
		return;
	}
	var sectionInfo = TemplateData.Control.getSection(controlInfo); 
	var row1 = SearchForm.Structure.Section.addRow(sectionInfo);
	var textInfo = SearchForm.Structure.Row.addControl(row1, '_custom', {
        FieldName: 'DocumentID',
        GroupName: '_custom',
        Title: 'Document ID',
        Type: 'tags',
    });
   	controlInfo.fields.push(textInfo.id);
}


var beforeApplyEditor = function(controlInfo){
	if(controlInfo.display != CONTROL_TYPE) {
		return;
	}
	var result = B2BEForm.DynamicSection.validate('#searchByOptionEditor');
	if(!_.isEmpty(result)) {
		return false;
	}

	var notUniq = B2BEForm.DynamicSection.validateUniqValue('#searchByOptionEditor');
	if(!_.isEmpty(notUniq)) {
		return false;
	}

	var values = _.values(B2BEForm.DynamicSection.getValues('#searchByOptionEditor'))
	controlInfo.options = values;
}

var showEditorUI = function(controlInfo, $formEditor){
	if(controlInfo.display != CONTROL_TYPE) {
		return;
	}
	Sidebar.Render.customFieldEditor(controlInfo.display);

	
	var objectList = GroupListConf.get({value: false});

	var option =  {
        initNumInstance: 1,
        uniqValue: ['group', 'field'],
        selectorAdd: '.add',
        selectorRemove: '.remove',
        // maxInstance: optionList.length,
        onMaxInstance: function () {
            B2BEToast.page_error();
        },
        onMinInstance: function () {
            B2BEToast.page_error();
        }
    };
	
	B2BEForm.DynamicSection.init('#searchByOptionEditor', '#searchByTemlate', objectList, option).done(function(){
		if(controlInfo.options) {
			B2BEForm.DynamicSection.setValues('#searchByOptionEditor', controlInfo.options);
		}
	})
}


export {SearchByHandler};