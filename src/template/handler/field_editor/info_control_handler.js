import {Sidebar} from 'template/ui/sidebar';

var CONTROL_TYPE = 'info';
var InfoControlHandler = {};


InfoControlHandler.init = function (Hooks) {
	Hooks.push('FieldEditor.afterActive', InfoControlHandler.showEditorUI);
	Hooks.push('FieldEditor.beforeApply', InfoControlHandler.applyEditor);
}


InfoControlHandler.applyEditor = function(controlInfo, values){
	if(controlInfo.display != CONTROL_TYPE) {
		return;
	}

	var v = B2BEForm.getValues('#fieldEditor .customWrapper');
	_.extend(values, v); // apply to attribute
}


InfoControlHandler.showEditorUI = function(controlInfo, $formEditor){
	if(controlInfo.display != CONTROL_TYPE) {
		return;
	}

	Sidebar.Render.customFieldEditor(controlInfo.display);

	B2BEForm.forceInit('#fieldEditor .customWrapper', {
		tooltip: {
			type: B2BEForm.OBJECT_TYPE.TEXTAREA, 
			rows: 3,
		}
	}).done(function($Form){
		$Form.setValues( {tooltip: controlInfo.tooltip})
	});
}


export {InfoControlHandler};