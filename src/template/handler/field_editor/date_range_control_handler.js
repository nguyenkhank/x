import {SearchForm} from 'template/ui/search_form';
import {Sidebar} from 'template/ui/sidebar';
import {TemplateData} from 'template/components/template_data';
import {ConfigData} from 'template/components/config_data';

var CONTROL_TYPE = 'datetimerange';
var DateRangeControlHandler = {};

DateRangeControlHandler.init = function (Hooks) {
	Hooks.push('SearchForm.Control.afterAdd', DateRangeControlHandler.afterAdd);
	Hooks.push('SearchForm.Control.afterRemove', DateRangeControlHandler.afterRemove);

	Hooks.push('FieldEditor.afterActive', DateRangeControlHandler.showEditorUI);
	Hooks.push('FieldEditor.beforeApply', DateRangeControlHandler.beforeApplyEditor);
}

DateRangeControlHandler.afterRemove = function(controlInfo){
	if(controlInfo.type !== CONTROL_TYPE) {
		return;
	}
	_.each(controlInfo.rows, function(rowID){
		SearchForm.Structure.Row.forceRemove(rowID); 
	});
}

DateRangeControlHandler.afterAdd = function(controlInfo){
	if(controlInfo.type !== CONTROL_TYPE) {
		return;
	}
	var sectionInfo = TemplateData.Control.getSection(controlInfo); 

	var row1 = SearchForm.Structure.Section.addRow(sectionInfo, {noAction: true});
	var row2 = SearchForm.Structure.Section.addRow(sectionInfo, {noAction: true});

	controlInfo.rows.push(row1.id);
	controlInfo.rows.push(row2.id);


	SearchForm.Structure.Row.addControl(row1, '_custom', {
        FieldName: 'DateFrom',
        GroupName: '_custom',
        Title: 'Date From',
        Type: 'date',
    });

    SearchForm.Structure.Row.addControl(row1, '_custom', {
        FieldName: 'TimeFrom',
        GroupName: '_custom',
        Title: 'Time From',
        Type: 'time',
    });

    SearchForm.Structure.Row.addControl(row2, '_custom', {
        FieldName: 'DateTo',
        GroupName: '_custom',
        Title: 'Date To',
        Type: 'date',
    });

    SearchForm.Structure.Row.addControl(row2, '_custom', {
        FieldName: 'TimeTo',
        GroupName: '_custom',
        Title: 'Time To',
        Type: 'time',
    });
}


DateRangeControlHandler.beforeApplyEditor = function(controlInfo){
	if(controlInfo.display != CONTROL_TYPE) {
		return;
	}
	var result = B2BEForm.DynamicSection.validate('#dateRangeOptionEditor');
	if(!_.isEmpty(result)) {
		return false;
	}

	var notUniq = B2BEForm.DynamicSection.validateUniqValue('#dateRangeOptionEditor');
	if(!_.isEmpty(notUniq)) {
		return false;
	}

	var values = _.values(B2BEForm.DynamicSection.getValues('#dateRangeOptionEditor'))
	controlInfo.options = values;
}

DateRangeControlHandler.showEditorUI = function(controlInfo, $formEditor){
	if(controlInfo.display != CONTROL_TYPE) {
		return;
	}
	Sidebar.Render.customFieldEditor(controlInfo.display);

	var dataList = ConfigData.getDateFields();
	var optionList = _.map(dataList, function(fieldConfig){
		return {
			id:   fieldConfig.GroupName + '.' + fieldConfig.FieldName + '.' + fieldConfig.Type,
			text: fieldConfig.GroupName + '.' + fieldConfig.FieldName
		}
	})
	

	var objectList =  {
		id: {
			type: B2BEForm.OBJECT_TYPE.SELECT2, 
			style: {width: '100%'},
			// multiple: true,
			optionList: optionList,
			required: true,
			events: {
				change: function(e){
					var value = e.moreInfo.value;
					if(!value) {
						return;
					}
					var controlInfo = e.moreInfo.object;

					var arr = value.split('.');
					var obj =  _.findWhere(dataList, { FieldName: arr[1], GroupName: arr[0] });
					B2BEForm.setValues(controlInfo.containerId, {text: obj.Title });
				}
			}
		},
		text: {
			type: B2BEForm.OBJECT_TYPE.TEXT,
			required: true,
		}
	};

	var option =  {
        initNumInstance: 1,
        uniqValue: ['id'],
        selectorAdd: '.add',
        selectorRemove: '.remove',
        maxInstance: dataList.length,
        onMaxInstance: function () {
            B2BEToast.page_error();
        },
        onMinInstance: function () {
            B2BEToast.page_error();
        }
    };
	
	B2BEForm.DynamicSection.init('#dateRangeOptionEditor', '#dateRangeTemlate', objectList, option).done(function(){
		if(controlInfo.options) {
			B2BEForm.DynamicSection.setValues('#dateRangeOptionEditor', controlInfo.options);
		}
	})
}


export {DateRangeControlHandler};