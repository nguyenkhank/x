import {Sidebar} from 'template/ui/sidebar';
import {GroupListConf} from 'template/handler/common/group_field_conf';


var CONTROL_TYPE = 'searchtype';

var SearchTypeHandler = {};


var showEditorUI = function(controlInfo){
	if(controlInfo.display != CONTROL_TYPE) {
		return;
	}
	Sidebar.Render.customFieldEditor(controlInfo.display);
	var objectList =  GroupListConf.get({value: true});

	var option =  {
        initNumInstance: 1,
        uniqValue: ['value'],
        selectorAdd: '.add',
        selectorRemove: '.remove',
        // maxInstance: optionList.length,
        onMaxInstance: function () {
            B2BEToast.page_error();
        },
        onMinInstance: function () {
            B2BEToast.page_error();
        }
    };
	
	B2BEForm.DynamicSection.init('#searchTypeOptionEditor', '#searchTypeTemlate', objectList, option).done(function(){
		if(controlInfo.options) {
			B2BEForm.DynamicSection.setValues('#searchTypeOptionEditor', controlInfo.options);
		}
	})

}

var beforeApplyEditor = function(controlInfo, $formEditor){
	if(controlInfo.display != CONTROL_TYPE) {
		return;
	}

	var result = B2BEForm.DynamicSection.validate('#searchTypeOptionEditor');
	if(!_.isEmpty(result)) {
		return false;
	}

	var notUniq = B2BEForm.DynamicSection.validateUniqValue('#searchTypeOptionEditor');
	if(!_.isEmpty(notUniq)) {
		return false;
	}

	var values = _.values(B2BEForm.DynamicSection.getValues('#searchTypeOptionEditor'))
	controlInfo.options = values;
}


SearchTypeHandler.init = function (Hooks) {
	Hooks.push('FieldEditor.afterActive', showEditorUI);
	Hooks.push('FieldEditor.beforeApply', beforeApplyEditor);
}

export {SearchTypeHandler}; 