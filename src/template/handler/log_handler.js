var LogHandler = {};

LogHandler.init = function (Hooks) {

	var list = [
		'ColumnEditor', 
		'ResultTable',
		'FieldEditor',
		'SearchForm.Section', 
		'SearchForm.Row', 
		'SearchForm.Control', 
	]

	_.each(list, function(path){
		var groupHook = _.accessStr(Hooks, path);

		_.each(groupHook, function(hookList, hookName){
			// push log into hooks
			hookList.push(function(){

				// console.log('%c Oh my heavens! ', 'background: #222; color: #bada55');
				var color = 'black';
				if( hookName.startsWith('before') ) {
					color = 'red';
				}

				if( hookName.startsWith('after') ) {
					color = 'green';
				}


				console.log(`%c [HOOK][${path}][${hookName}]:`, 'color: ' + color, Array.from(arguments));
			}, 100); // low priority
			// end
		})

	});

}

export {LogHandler};