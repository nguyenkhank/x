import {ConfigData} from 'template/components/config_data';

var GroupListConf = {};

var groupControl = function(){
	var optionList = ConfigData.groupDropdown();
	return {
		type: B2BEForm.OBJECT_TYPE.SELECT2, 
		style: {width: '100%'},
		// multiple: true,
		optionList: optionList,
		required: true,
		events: {
			change: function(e){
				var value = e.moreInfo.value;
				if(!value) {
					return;
				}
				// set datasource field
				var controlInfo = e.moreInfo.object;
				var fieldSource = ConfigData.fieldDropdown(value);
				B2BEForm.setDataSource(controlInfo.containerId, 'field', fieldSource);
			}
		}
	};
}

var fieldControl = function(){
	return  {
		type: B2BEForm.OBJECT_TYPE.SELECT2, 
		style: {width: '100%'},
		// multiple: true,
		optionList: [],
		required: true,
		events: {
			change: function(e){
				var value = e.moreInfo.value;

				var controlInfo = e.moreInfo.object;
				if(!value) {
					B2BEForm.setValues(controlInfo.containerId, {text: '' });
					return;
				}
				var values = B2BEForm.getValues(controlInfo.containerId);
				var obj = ConfigData.getFieldConfig(values.group, values.field);
				B2BEForm.setValues(controlInfo.containerId, {text: obj.Title });
			}
		}
	};
}

var textControl = function(){
	return  {
		type: B2BEForm.OBJECT_TYPE.TEXT,
		required: true,
	};
}


var valueControl = function(){
	return {
		type: B2BEForm.OBJECT_TYPE.TEXT,
		required: true,
	};
}

GroupListConf.get = function(conf){
	var groupListConf =  {};

	groupListConf.group = groupControl();
	groupListConf.field = fieldControl();
	groupListConf.text = textControl();

	if(conf.value) {
		groupListConf.value = valueControl();
	}

	return groupListConf;
}

export { GroupListConf }