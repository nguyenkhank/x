import {ConfigData} from 'template/components/config_data';
import {TemplateData} from 'template/components/template_data';
import {FIXED_TYPES, CONTROL_ATTR_DEFAULT} from 'template/components/constants';
import {SearchForm} from 'template/ui/search_form';

var handler = {
	Section: {
		autoAddRow:  function(sectionInfo){;
			return SearchForm.Structure.Section.addRow(sectionInfo);
		}
	},
	Control: {
		moreInfo: function(basicInfo, fieldConfig, rowInfo) {
			basicInfo.display  = 'tags';

			if(ConfigData.hasDropdown(basicInfo.group, basicInfo.name)) {
				var dropdownConfig = ConfigData.getDropdown(basicInfo.group, basicInfo.name);

				basicInfo.sourceGroup = dropdownConfig.SourceGroup;
				basicInfo.columnSelected = dropdownConfig.ColumnSelected;
				basicInfo.displayFormat = dropdownConfig.DisplayFormat;		
				basicInfo.display =  'select';
			} else if(_.contains(FIXED_TYPES, fieldConfig.Type)) {
				basicInfo.display = fieldConfig.Type;
			}

			// default attribute
			if(_.has(CONTROL_ATTR_DEFAULT, basicInfo.display)) {
				_.extend(basicInfo, CONTROL_ATTR_DEFAULT[basicInfo.display]);
			}

			// more info about group
			if(ConfigData.hasGroupRequired(basicInfo.group)) {
				basicInfo.through_group = ConfigData.getGroupRequired(basicInfo.group);
			}
		}
	},
	Row: {
		prevent: function(rowInfo){;
			var numControls = TemplateData.Row.countControls(rowInfo);
			if(numControls > 0) {
				B2BEToast.form_error(text('Cannot delete Row has Control(s).'));
				return false;
			}
		}
	}
}

var SearchFormHandler = {};

SearchFormHandler.init = function(Hooks) {

	Hooks.push('SearchForm.Control.beforeAdd', handler.Control.moreInfo);

	// validate remove Row
	Hooks.push('SearchForm.Row.beforeRemove', handler.Row.prevent);

	// add row after register section
	Hooks.push('SearchForm.Section.afterRegister', handler.Section.autoAddRow);
}


export {SearchFormHandler};