import {LABEL_TYPES, FIXED_TYPES} from 'template/components/constants';
import {Service as AjaxService} from 'template/components/ajax_service';
import {ConfigData} from 'template/components/config_data';

import {InfoControlHandler} from './field_editor/info_control_handler';
import {DateRangeControlHandler} from './field_editor/date_range_control_handler';
import {SearchByHandler} from './field_editor/search_by_handler';
import {SearchTypeHandler} from './field_editor/search_type_handler';


var FieldEditorHandler = {};

FieldEditorHandler.init = function(Hooks){
	Hooks.push('FieldEditor.afterActive', FieldEditorHandler.setDatasourceDisplay);
	Hooks.push('FieldEditor.afterActive', FieldEditorHandler.setDatasourceEnum);

	InfoControlHandler.init(Hooks);
	DateRangeControlHandler.init(Hooks);
	SearchByHandler.init(Hooks);
	SearchTypeHandler.init(Hooks);
}

FieldEditorHandler.setDatasourceDisplay = function(controlInfo, $formEditor){
	var optionDisplay = [
	    {id: 'text', text: text('Text') }, 
	    {id: 'tags', text: text('Tags')}, 
	    {id: 'enum', text: text('Enum')}, 
	];

	if(ConfigData.hasDropdown(controlInfo.group, controlInfo.name)) {
		optionDisplay.push({id: 'select', text: text('Select') });
	}

	// FOR SOME CONTROL FIXED TYPES
	if(_.contains(FIXED_TYPES, controlInfo.display)) {
		var obj = {};
		obj.id = controlInfo.display;
		obj.text  = LABEL_TYPES[controlInfo.display];
		optionDisplay = [obj];
	}
	// END

	$formEditor.setDataSource('display', optionDisplay);
	$formEditor.setValues({display: controlInfo.display});
}

FieldEditorHandler.setDatasourceEnum = function(controlInfo, $formEditor){
	if(controlInfo.type != 'enum' ) {
		return;
	}
	if(_.has(controlInfo, 'dataList') && !_.isEmpty(controlInfo.dataList)) {
		return;
	}
	AjaxService.getEnumValues(controlInfo.group, controlInfo.name).done(function(response){
		if(!response) {
			return;
		}
		var datasource = _.map(response, function(item){
			return { id: item, text: item, };
		})
		$formEditor.setValues({'dataList': datasource});
	});
}


export {FieldEditorHandler};