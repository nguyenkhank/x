import {ConfigData} from 'template/components/config_data';
import {TemplateData} from 'template/components/template_data';
import {Hooks} from 'template/components/hooks';
import {Core} from 'template/core';
import {Render} from 'template/ui/render';
import {SearchForm} from 'template/ui/search_form';
import {StyleBuilder} from 'base/style_builder';

var instance = {};


instance.setData = function(data) {
    // clear 
    Render.draw(data);
}

instance.getData = function(){
    return {
        sections: TemplateData.Section.getData(),
        rows: TemplateData.Row.getData(),
        controls: TemplateData.Control.getData(),
        columns: TemplateData.Column.getData(),
    }
}

instance.init = function(option){
    Core.init(option);
}

instance.SearchForm = SearchForm;

window.SearchMaintenance = instance;