var RenderFactory = {};


RenderFactory.create = function () {
	var inst = {};

	inst.buildTemplate = function(templateString, data){
		var templ = _.template(templateString);

		try{
			var html = templ(data);
		} catch(e) {
			klog(e)
		}
		
		return html; 
	}

	return inst;
}

export {RenderFactory};