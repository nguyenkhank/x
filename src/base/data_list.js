class DataList   {
  	constructor(bundle) {
    	this.bundle = bundle;
  	}

  	setAttribute(info, attr, value) {
  		var objInfo = this.get(info);
		objInfo[attr] = value;
  	}

  	setOrder(info, order){
  		this.setAttribute(info, 'order', order);
	}

	remove(info){
		var objInfo = this.get(info);
    // klog(objInfo)
		var listAfter = _.reject(this.bundle, function(obj){ 
        return (objInfo.id == obj.id); 
    });
    this.bundle = listAfter;
	}

	getByID(id) {
  	return _.findWhere(this.bundle, {id: id});
	}

	setData(data) {
		this.bundle = data;
	}

	getData(){
		return this.bundle;
	}

	get(info) {
		if(_.isString(info)) {
  		return this.getByID(info);
  	}
  	if(_.isObject(info)) {
  		if(_.has(info, 'id')) {
  			return this.getByID(info.id);
  		}
  	}
  	return null;
	}
}

export {DataList}