var ConfigDataFactory = {};


ConfigDataFactory.create = function (option) {
	var inst = {};

	inst.setUserOption = function(userOption){
		_.extend(option, userOption);
	}

	return inst;
}

export {ConfigDataFactory};