// display_date_format is an global variable
var displayDate = function(val){ // val is from database 
	var format = datepicker_format_to_moment_format(display_date_format); 
	return moment(val).format(format);
}

var displayDatetime = function(val) {
	return moment(val).format(display_datetime_moment_format);
}

var dbDate = function(val){
	return moment(val, display_datetime_moment_format).format("DD-MM-YYYY");
}

var dbDatetime = function(val){
	return moment(val, display_datetime_moment_format).format("DD-MM-YYYY HH:mm:ss");
}


export {
	displayDate,
	displayDatetime,
	dbDate,
	dbDatetime,
}