var StyleBuilder = {};

StyleBuilder.BIU = function (bold, italic, underline) {
    var res = ''; 
    if(bold) {
        res += 'font-weight: bold;'
    }
    if(italic) {
        res += 'font-style:italic;'
    }
    if(underline) {
        res += 'text-decoration: underline;'
    }
    return res;
}



window.StyleBuilder = StyleBuilder;
export {StyleBuilder}