import {Core} from 'gui/core';
import {Render} from 'gui/ui/render';
import {StyleBuilder} from 'base/style_builder';

import {UI} from 'gui/ui';

var instance = {};

instance.setDocumentID = function(documentID){
	UI.setDocumentID(documentID);
}

instance.draw = function (data) {
	UI.draw(data);
}

instance.init = function(option){
	Core.init(option);
}

window.Search3 = instance;