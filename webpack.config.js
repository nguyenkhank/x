var webpack = require('webpack');
var path = require('path');
//Thư mục sẽ chứa tập tin được biên dịch
var BUILD_DIR = path.resolve(__dirname, '../../assets/js/modules/search3/');
//Thư mục chứa dự án - các component React
var APP_DIR = path.resolve(__dirname, 'src');
 
var config = {
  entry: {
    template: APP_DIR + '/template.js',
    gui: APP_DIR + '/gui.js',
  },
  output: {
    path: BUILD_DIR,
    filename: '[name]_bundle.js'
  },
//Thêm 
  module : {
    loaders : [
      {
        test : /\.js?/,
        include : APP_DIR,
        loader : 'babel-loader'
      }
    ]
  },
  //Kết thúc Thêm
  resolve: {
     modules: [
        "node_modules",
        APP_DIR
    ],
  },
};
 
module.exports = config;